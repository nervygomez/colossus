require('dotenv').config()
const www = require('./config/www.js')
const app = require('./app')
const models = require('./models')
// const init = require('./init')
const migrator = require('./migrator')

models.sequelize
  .authenticate()
  .then(() => {
    console.log(`Connection to the database ${models.sequelize.getDatabaseName()} has been established successfully.`)
    migrator.autoMigrate()
      .then(() => {
        app.listen(www.port, () => {
          console.log(`API listening on ${www.url}:${www.port}`)
        })
      })
      .catch(err => { console.error(err) })
  }).catch(err => { console.error('Unable to connect to the database:', err) })

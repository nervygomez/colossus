CREATE TABLE public."duracionGarantia" (
    id integer NOT NULL,
    dia integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
    );
CREATE SEQUENCE public."duracionGarantia_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."duracionGarantia_id_seq" OWNED BY public."duracionGarantia".id;
ALTER TABLE ONLY public."duracionGarantia" ALTER COLUMN id SET DEFAULT nextval('public."duracionGarantia_id_seq"'::regclass);
ALTER TABLE ONLY public."duracionGarantia"
    ADD CONSTRAINT "duracionGarantia_pkey" PRIMARY KEY (id);

INSERT INTO "duracionGarantia" (dia, "createdAt", "updatedAt") values(15, NOW(), Now());
INSERT INTO "duracionGarantia" (dia, "createdAt", "updatedAt") values(30, NOW(), Now());
INSERT INTO "duracionGarantia" (dia, "createdAt", "updatedAt") values(60, NOW(), Now());
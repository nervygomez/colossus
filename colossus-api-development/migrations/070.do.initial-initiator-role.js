const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const contrasenaJefe = require('../config/env').jefePass
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage
var bcrypt = require('bcryptjs')

module.exports.generateSql = function () {
  const salt = bcrypt.genSaltSync(10)
  const contrasena = bcrypt.hashSync(contrasenaJefe, salt)

  return (
    `
    INSERT INTO "rol" (nombre, "tipoRol", "createdAt", "updatedAt", "esJefe") values ('Iniciador', 'administrativo', NOW(), NOW(), FALSE);

    INSERT INTO "permisoRol" ("createdAt", "updatedAt","permisoId", "rolId") select NOW(), NOW(), p."id", r.id 
      from permiso p, rol r
      where r.nombre='Iniciador' and p.nombre='configuracion';
    
    INSERT INTO "usuario" (nombre, apellido, correo, sexo, "idRol", "urlFoto", contrasena,"fechaNacimiento", "createdAt", "updatedAt") 
    select 'Iniciador', 'Siri', 'iniciador@siri.com', 'M', r.id,
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', '${contrasena}',
      NOW(), NOW(), NOW() from rol r where nombre='Iniciador' ON CONFLICT (id) DO NOTHING;

    `
  )
}

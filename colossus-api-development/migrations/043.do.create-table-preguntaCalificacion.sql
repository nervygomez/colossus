CREATE TABLE public."preguntaCalificacion" (
    id integer NOT NULL,
    descripcion character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."preguntaCalificacion_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
    ALTER SEQUENCE public."preguntaCalificacion_id_seq" OWNED BY public."preguntaCalificacion".id;
    ALTER TABLE ONLY public."preguntaCalificacion" ALTER COLUMN id SET DEFAULT nextval('public."preguntaCalificacion_id_seq"'::regclass);
ALTER TABLE ONLY public."preguntaCalificacion"
    ADD CONSTRAINT "preguntaCalificacion_pkey" PRIMARY KEY (id);
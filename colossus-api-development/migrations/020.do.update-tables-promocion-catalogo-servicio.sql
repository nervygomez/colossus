ALTER TABLE ONLY public.promocion
  ADD COLUMN "fechaInicio" timestamp with time zone;
ALTER TABLE ONLY public.promocion
  ADD COLUMN "fechaExpiracion" timestamp with time zone;
ALTER TABLE ONLY public.promocion
  ADD COLUMN "idCatalogoServicio" integer REFERENCES PUBLIC."catalogoServicio"(id);
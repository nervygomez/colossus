const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const contrasenaJefe = require('../config/env').jefePass
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage
var bcrypt = require('bcryptjs')

module.exports.generateSql = function () {
  const salt = bcrypt.genSaltSync(10)
  const contrasena = bcrypt.hashSync(contrasenaJefe, salt)

  return (
    `
    INSERT INTO "usuario" (nombre, apellido, correo, sexo, "idRol", "urlFoto", contrasena,"fechaNacimiento", "createdAt", "updatedAt") 
    select 'Colossus', 'Siri', 'colossus@siri.com', 'M', r.id,
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}', '${contrasena}',
      NOW(), NOW(), NOW() from rol r where "esJefe"=true ON CONFLICT (id) DO NOTHING;  

    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Primera caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
       NOW(), NOW()) ON CONFLICT (id) DO NOTHING;

    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Segunda caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
      NOW(), NOW()) ON CONFLICT (id) DO NOTHING;
    
    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Tercera caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
      NOW(), NOW()) ON CONFLICT (id) DO NOTHING;


    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Cuarta caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
      NOW(), NOW()) ON CONFLICT (id) DO NOTHING; 

    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Quinta caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
      NOW(), NOW()) ON CONFLICT (id) DO NOTHING; 

    INSERT INTO "caracteristicaEmpresa" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Sexta caracteristica', 'Por favor agregue una descripcion, titulo e imagen que represente las fortalezas de su empresa',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
      NOW(), NOW()) ON CONFLICT (id) DO NOTHING; 
    `
  )
}

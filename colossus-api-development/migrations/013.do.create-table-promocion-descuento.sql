CREATE TABLE PUBLIC.descuento (
  id SERIAL PRIMARY KEY,
  nombre character varying(255),
  porcentaje float,
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone
);

CREATE TABLE PUBLIC.promocion (
  id SERIAL PRIMARY KEY,
  nombre character varying(255),
  descripcion character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone,
  "idDescuento" integer REFERENCES PUBLIC.descuento(id)
);
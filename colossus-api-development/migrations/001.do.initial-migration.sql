
CREATE TABLE public.actividad (
    id integer NOT NULL,
    nombre character varying(255),
    descripcion character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idCatalogoServicio" integer
);
CREATE SEQUENCE public.actividad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.actividad_id_seq OWNED BY public.actividad.id;
CREATE TABLE public.agenda (
    id integer NOT NULL,
    descripcion character varying(255),
    estatus character varying(255) DEFAULT 'E'::character varying,
    origen character varying(255),
    "fechaActividad" date NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idOrigen" integer,
    "idBloqueHorario" integer,
    "idUsuario" integer,
    "idOrdenServicio" integer
);
CREATE SEQUENCE public.agenda_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.agenda_id_seq OWNED BY public.agenda.id;
CREATE TABLE public."bloqueHorario" (
    id integer NOT NULL,
    "horaInicio" time without time zone,
    "horaFinal" time without time zone NOT NULL,
    descripcion character varying(255) NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."bloqueHorario_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."bloqueHorario_id_seq" OWNED BY public."bloqueHorario".id;
CREATE TABLE public."caracteristicaCliente" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idTipoCaracteristicaCliente" integer
);
CREATE TABLE public."caracteristicaClienteUsuario" (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "caracteristicaClienteId" integer NOT NULL,
    "usuarioId" integer NOT NULL
);
CREATE SEQUENCE public."caracteristicaCliente_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."caracteristicaCliente_id_seq" OWNED BY public."caracteristicaCliente".id;
CREATE TABLE public."caracteristicaEmpresa" (
    id integer NOT NULL,
    titulo character varying(255),
    descripcion character varying(255),
    icono character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."caracteristicaEmpresa_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."caracteristicaEmpresa_id_seq" OWNED BY public."caracteristicaEmpresa".id;
CREATE TABLE public.carrusel (
    id integer NOT NULL,
    "urlImagen" character varying(2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public.carrusel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.carrusel_id_seq OWNED BY public.carrusel.id;
CREATE TABLE public."catalogoServicio" (
    id integer NOT NULL,
    descripcion character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idTipoEquipo" integer,
    "idTipoServicio" integer
);
CREATE SEQUENCE public."catalogoServicio_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."catalogoServicio_id_seq" OWNED BY public."catalogoServicio".id;
CREATE TABLE public."categoriaTipoEquipo" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."categoriaTipoEquipo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."categoriaTipoEquipo_id_seq" OWNED BY public."categoriaTipoEquipo".id;
CREATE TABLE public.empresa (
    id integer NOT NULL,
    nombre character varying(255),
    telefono character varying(255),
    direccion character varying(255),
    correo character varying(255),
    "urlLogo" character varying(2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public.empresa_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.empresa_id_seq OWNED BY public.empresa.id;
CREATE TABLE public.especialidad (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "usuarioId" integer NOT NULL,
    "catalogoServicioId" integer NOT NULL
);
CREATE TABLE public.marca (
    id integer NOT NULL,
    nombre character varying(255),
    "urlImagen" character varying(2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public.marca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.marca_id_seq OWNED BY public.marca.id;
CREATE TABLE public.modeloequipo (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idMarca" integer,
    "idTipoEquipo" integer
);
CREATE SEQUENCE public.modeloequipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.modeloequipo_id_seq OWNED BY public.modeloequipo.id;
CREATE TABLE public."ordenServicio" (
    id integer NOT NULL,
    "fechaRespuestaCliente" date,
    "fechaFinalizacion" date,
    "fechaEntrega" date,
    "fechaRespuestaReclamo" date,
    "fechaFinalizacionReclamo" date,
    "fechaEntregaReclamo" date,
    estatus character varying(255) DEFAULT 'Invisible'::character varying,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idSolicitudServicio" integer,
    "idUsuario" integer
);
CREATE SEQUENCE public."ordenServicio_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."ordenServicio_id_seq" OWNED BY public."ordenServicio".id;
CREATE TABLE public.perfil (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "usuarioId" integer NOT NULL,
    "caracteristicaClienteId" integer NOT NULL
);
CREATE TABLE public.permiso (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE TABLE public."permisoRol" (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "permisoId" integer NOT NULL,
    "rolId" integer NOT NULL
);
CREATE SEQUENCE public.permiso_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.permiso_id_seq OWNED BY public.permiso.id;
CREATE TABLE public.revision (
    id integer NOT NULL,
    costo integer,
    diagnostico character varying(255) DEFAULT NULL::character varying,
    "estatusEquipo" character varying(255) DEFAULT 'E'::character varying,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idSolicitudServicio" integer
);
CREATE SEQUENCE public.revision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.revision_id_seq OWNED BY public.revision.id;
CREATE TABLE public.rol (
    id integer NOT NULL,
    nombre character varying(255),
    "tipoRol" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public.rol_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.rol_id_seq OWNED BY public.rol.id;
CREATE TABLE public."sliderMovil" (
    id integer NOT NULL,
    titulo character varying(255),
    descripcion character varying(255),
    icono character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."sliderMovil_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."sliderMovil_id_seq" OWNED BY public."sliderMovil".id;
CREATE TABLE public."solicitudServicio" (
    id integer NOT NULL,
    "fechaRespuesta" timestamp with time zone,
    descripcion character varying(255),
    "motivoRechazo" character varying(255) DEFAULT NULL::character varying,
    "fechaRevisado" timestamp with time zone,
    "fechaGeneracionPresupuesto" timestamp with time zone,
    presupuesto integer,
    "estatusPresupuesto" character varying(255) DEFAULT NULL::character varying,
    estatus character varying(255) DEFAULT 'E'::character varying,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idCatalogoServicio" integer,
    "idModeloEquipo" integer,
    "idUsuario" integer
);
CREATE SEQUENCE public."solicitudServicio_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."solicitudServicio_id_seq" OWNED BY public."solicitudServicio".id;
CREATE TABLE public.tarea (
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "actividadId" integer NOT NULL,
    "ordenServicioId" integer NOT NULL
);
CREATE TABLE public."tipoCaracteristicaCliente" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."tipoCaracteristicaCliente_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoCaracteristicaCliente_id_seq" OWNED BY public."tipoCaracteristicaCliente".id;
CREATE TABLE public."tipoEquipo" (
    id integer NOT NULL,
    nombre character varying(255),
    "urlImagen" character varying(2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idCategoriaTipoEquipo" integer
);
CREATE SEQUENCE public."tipoEquipo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoEquipo_id_seq" OWNED BY public."tipoEquipo".id;
CREATE TABLE public."tipoIncidencia" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."tipoIncidencia_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoIncidencia_id_seq" OWNED BY public."tipoIncidencia".id;
CREATE TABLE public."tipoReclamo" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."tipoReclamo_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoReclamo_id_seq" OWNED BY public."tipoReclamo".id;
CREATE TABLE public."tipoServicio" (
    id integer NOT NULL,
    nombre character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
CREATE SEQUENCE public."tipoServicio_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public."tipoServicio_id_seq" OWNED BY public."tipoServicio".id;
CREATE TABLE public.usuario (
    id integer NOT NULL,
    nombre character varying(255),
    apellido character varying(255),
    correo character varying(255),
    direccion text,
    telefono character varying(255),
    "fechaNacimiento" date,
    contrasena character varying(255),
    "resetToken" character varying(255),
    sexo character varying(1),
    "urlFoto" character varying(2000),
    "documentoIdentidad" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idRol" integer
);
CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;
ALTER TABLE ONLY public.actividad ALTER COLUMN id SET DEFAULT nextval('public.actividad_id_seq'::regclass);
ALTER TABLE ONLY public.agenda ALTER COLUMN id SET DEFAULT nextval('public.agenda_id_seq'::regclass);
ALTER TABLE ONLY public."bloqueHorario" ALTER COLUMN id SET DEFAULT nextval('public."bloqueHorario_id_seq"'::regclass);
ALTER TABLE ONLY public."caracteristicaCliente" ALTER COLUMN id SET DEFAULT nextval('public."caracteristicaCliente_id_seq"'::regclass);
ALTER TABLE ONLY public."caracteristicaEmpresa" ALTER COLUMN id SET DEFAULT nextval('public."caracteristicaEmpresa_id_seq"'::regclass);
ALTER TABLE ONLY public.carrusel ALTER COLUMN id SET DEFAULT nextval('public.carrusel_id_seq'::regclass);
ALTER TABLE ONLY public."catalogoServicio" ALTER COLUMN id SET DEFAULT nextval('public."catalogoServicio_id_seq"'::regclass);
ALTER TABLE ONLY public."categoriaTipoEquipo" ALTER COLUMN id SET DEFAULT nextval('public."categoriaTipoEquipo_id_seq"'::regclass);
ALTER TABLE ONLY public.empresa ALTER COLUMN id SET DEFAULT nextval('public.empresa_id_seq'::regclass);
ALTER TABLE ONLY public.marca ALTER COLUMN id SET DEFAULT nextval('public.marca_id_seq'::regclass);
ALTER TABLE ONLY public.modeloequipo ALTER COLUMN id SET DEFAULT nextval('public.modeloequipo_id_seq'::regclass);
ALTER TABLE ONLY public."ordenServicio" ALTER COLUMN id SET DEFAULT nextval('public."ordenServicio_id_seq"'::regclass);
ALTER TABLE ONLY public.permiso ALTER COLUMN id SET DEFAULT nextval('public.permiso_id_seq'::regclass);
ALTER TABLE ONLY public.revision ALTER COLUMN id SET DEFAULT nextval('public.revision_id_seq'::regclass);
ALTER TABLE ONLY public.rol ALTER COLUMN id SET DEFAULT nextval('public.rol_id_seq'::regclass);
ALTER TABLE ONLY public."sliderMovil" ALTER COLUMN id SET DEFAULT nextval('public."sliderMovil_id_seq"'::regclass);
ALTER TABLE ONLY public."solicitudServicio" ALTER COLUMN id SET DEFAULT nextval('public."solicitudServicio_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoCaracteristicaCliente" ALTER COLUMN id SET DEFAULT nextval('public."tipoCaracteristicaCliente_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoEquipo" ALTER COLUMN id SET DEFAULT nextval('public."tipoEquipo_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoIncidencia" ALTER COLUMN id SET DEFAULT nextval('public."tipoIncidencia_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoReclamo" ALTER COLUMN id SET DEFAULT nextval('public."tipoReclamo_id_seq"'::regclass);
ALTER TABLE ONLY public."tipoServicio" ALTER COLUMN id SET DEFAULT nextval('public."tipoServicio_id_seq"'::regclass);
ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);
ALTER TABLE ONLY public.actividad
    ADD CONSTRAINT actividad_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT agenda_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."bloqueHorario"
    ADD CONSTRAINT "bloqueHorario_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."caracteristicaClienteUsuario"
    ADD CONSTRAINT "caracteristicaClienteUsuario_pkey" PRIMARY KEY ("caracteristicaClienteId", "usuarioId");
ALTER TABLE ONLY public."caracteristicaCliente"
    ADD CONSTRAINT "caracteristicaCliente_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."caracteristicaEmpresa"
    ADD CONSTRAINT "caracteristicaEmpresa_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public.carrusel
    ADD CONSTRAINT carrusel_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."catalogoServicio"
    ADD CONSTRAINT "catalogoServicio_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."categoriaTipoEquipo"
    ADD CONSTRAINT "categoriaTipoEquipo_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public.empresa
    ADD CONSTRAINT empresa_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.especialidad
    ADD CONSTRAINT especialidad_pkey PRIMARY KEY ("usuarioId", "catalogoServicioId");
ALTER TABLE ONLY public.marca
    ADD CONSTRAINT marca_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.modeloequipo
    ADD CONSTRAINT modeloequipo_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."ordenServicio"
    ADD CONSTRAINT "ordenServicio_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY ("usuarioId", "caracteristicaClienteId");
ALTER TABLE ONLY public."permisoRol"
    ADD CONSTRAINT "permisoRol_pkey" PRIMARY KEY ("permisoId", "rolId");
ALTER TABLE ONLY public.permiso
    ADD CONSTRAINT permiso_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.revision
    ADD CONSTRAINT revision_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public."sliderMovil"
    ADD CONSTRAINT "sliderMovil_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."solicitudServicio"
    ADD CONSTRAINT "solicitudServicio_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public.tarea
    ADD CONSTRAINT tarea_pkey PRIMARY KEY ("actividadId", "ordenServicioId");
ALTER TABLE ONLY public."tipoCaracteristicaCliente"
    ADD CONSTRAINT "tipoCaracteristicaCliente_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."tipoEquipo"
    ADD CONSTRAINT "tipoEquipo_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."tipoIncidencia"
    ADD CONSTRAINT "tipoIncidencia_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."tipoReclamo"
    ADD CONSTRAINT "tipoReclamo_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public."tipoServicio"
    ADD CONSTRAINT "tipoServicio_pkey" PRIMARY KEY (id);
ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_correo_key UNIQUE (correo);
ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT "usuario_documentoIdentidad_key" UNIQUE ("documentoIdentidad");
ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.actividad
    ADD CONSTRAINT "actividad_idCatalogoServicio_fkey" FOREIGN KEY ("idCatalogoServicio") REFERENCES public."catalogoServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT "agenda_idBloqueHorario_fkey" FOREIGN KEY ("idBloqueHorario") REFERENCES public."bloqueHorario"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT "agenda_idOrdenServicio_fkey" FOREIGN KEY ("idOrdenServicio") REFERENCES public."ordenServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.agenda
    ADD CONSTRAINT "agenda_idUsuario_fkey" FOREIGN KEY ("idUsuario") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."caracteristicaClienteUsuario"
    ADD CONSTRAINT "caracteristicaClienteUsuario_caracteristicaClienteId_fkey" FOREIGN KEY ("caracteristicaClienteId") REFERENCES public."caracteristicaCliente"(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public."caracteristicaClienteUsuario"
    ADD CONSTRAINT "caracteristicaClienteUsuario_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public."caracteristicaCliente"
    ADD CONSTRAINT "caracteristicaCliente_idTipoCaracteristicaCliente_fkey" FOREIGN KEY ("idTipoCaracteristicaCliente") REFERENCES public."tipoCaracteristicaCliente"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."catalogoServicio"
    ADD CONSTRAINT "catalogoServicio_idTipoEquipo_fkey" FOREIGN KEY ("idTipoEquipo") REFERENCES public."tipoEquipo"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."catalogoServicio"
    ADD CONSTRAINT "catalogoServicio_idTipoServicio_fkey" FOREIGN KEY ("idTipoServicio") REFERENCES public."tipoServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.especialidad
    ADD CONSTRAINT "especialidad_catalogoServicioId_fkey" FOREIGN KEY ("catalogoServicioId") REFERENCES public."catalogoServicio"(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.especialidad
    ADD CONSTRAINT "especialidad_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.modeloequipo
    ADD CONSTRAINT "modeloequipo_idMarca_fkey" FOREIGN KEY ("idMarca") REFERENCES public.marca(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.modeloequipo
    ADD CONSTRAINT "modeloequipo_idTipoEquipo_fkey" FOREIGN KEY ("idTipoEquipo") REFERENCES public."tipoEquipo"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."ordenServicio"
    ADD CONSTRAINT "ordenServicio_idSolicitudServicio_fkey" FOREIGN KEY ("idSolicitudServicio") REFERENCES public."solicitudServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."ordenServicio"
    ADD CONSTRAINT "ordenServicio_idUsuario_fkey" FOREIGN KEY ("idUsuario") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT "perfil_caracteristicaClienteId_fkey" FOREIGN KEY ("caracteristicaClienteId") REFERENCES public."caracteristicaCliente"(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT "perfil_usuarioId_fkey" FOREIGN KEY ("usuarioId") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public."permisoRol"
    ADD CONSTRAINT "permisoRol_permisoId_fkey" FOREIGN KEY ("permisoId") REFERENCES public.permiso(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public."permisoRol"
    ADD CONSTRAINT "permisoRol_rolId_fkey" FOREIGN KEY ("rolId") REFERENCES public.rol(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.revision
    ADD CONSTRAINT "revision_idSolicitudServicio_fkey" FOREIGN KEY ("idSolicitudServicio") REFERENCES public."solicitudServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."solicitudServicio"
    ADD CONSTRAINT "solicitudServicio_idCatalogoServicio_fkey" FOREIGN KEY ("idCatalogoServicio") REFERENCES public."catalogoServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."solicitudServicio"
    ADD CONSTRAINT "solicitudServicio_idModeloEquipo_fkey" FOREIGN KEY ("idModeloEquipo") REFERENCES public.modeloequipo(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public."solicitudServicio"
    ADD CONSTRAINT "solicitudServicio_idUsuario_fkey" FOREIGN KEY ("idUsuario") REFERENCES public.usuario(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.tarea
    ADD CONSTRAINT "tarea_actividadId_fkey" FOREIGN KEY ("actividadId") REFERENCES public.actividad(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.tarea
    ADD CONSTRAINT "tarea_ordenServicioId_fkey" FOREIGN KEY ("ordenServicioId") REFERENCES public."ordenServicio"(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public."tipoEquipo"
    ADD CONSTRAINT "tipoEquipo_idCategoriaTipoEquipo_fkey" FOREIGN KEY ("idCategoriaTipoEquipo") REFERENCES public."categoriaTipoEquipo"(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT "usuario_idRol_fkey" FOREIGN KEY ("idRol") REFERENCES public.rol(id) ON UPDATE CASCADE ON DELETE SET NULL;


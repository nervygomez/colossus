CREATE TABLE public.bitacora (
    id integer NOT NULL,
    "fechaMovimiento"  timestamp with time zone,
    descripcion character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idSolicitudServicio" integer
);
CREATE SEQUENCE public.bitacora_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.bitacora_id_seq OWNED BY public.bitacora.id;
ALTER TABLE ONLY public.bitacora ALTER COLUMN id SET DEFAULT nextval('public.bitacora_id_seq'::regclass);
ALTER TABLE ONLY public.bitacora
    ADD CONSTRAINT bitacora_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.bitacora
    ADD CONSTRAINT "bitacora_idSolicitudServicio_fkey" FOREIGN KEY ("idSolicitudServicio") REFERENCES public."solicitudServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
    

CREATE TABLE public."descargaApp" (
    id SERIAL PRIMARY KEY,
    "titulo" character varying (255),
    "descripcion" character varying (255),
    "urlImagen" character varying(2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);


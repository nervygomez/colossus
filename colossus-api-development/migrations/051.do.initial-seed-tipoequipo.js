const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaults = require('../config/path.json').defaults
const staticRoot = require('../config/path.json').staticRoot

const path = require('path')

module.exports.generateSql = function () {
  if (process.env.COLOSSUS_SEED !== 'true') { return ('') } // do nothing

  return (
    `
    INSERT INTO "tipoEquipo"  
    (nombre, "idCategoriaTipoEquipo", "urlImagen", "createdAt", "updatedAt")
    select 'Smartphones', c.id, 
    '${new URL(path.join(staticRoot, defaults, 'telefonia.jpg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "categoriaTipoEquipo" c where nombre='Telefonía' ON CONFLICT (id) DO NOTHING;  

    INSERT INTO "tipoEquipo"  
    (nombre, "idCategoriaTipoEquipo", "urlImagen", "createdAt", "updatedAt")
    select 'Laptops', c.id, 
    '${new URL(path.join(staticRoot, defaults, 'computacion.jpg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "categoriaTipoEquipo" c where nombre='Computación' ON CONFLICT (id) DO NOTHING;  

    INSERT INTO "tipoEquipo"  
    (nombre, "idCategoriaTipoEquipo", "urlImagen", "createdAt", "updatedAt")
    select 'Consolas', c.id, 
    '${new URL(path.join(staticRoot, defaults, 'videojuegos.jpeg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "categoriaTipoEquipo" c where nombre='Videojuegos' ON CONFLICT (id) DO NOTHING;  
    `
  )
}

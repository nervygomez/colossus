ALTER TABLE ONLY public.rol
  ADD COLUMN "esJefe" boolean DEFAULT FALSE;

UPDATE "rol" set "esJefe"=TRUE, "tipoRol"='administrativo' where nombre='Jefe';

CREATE TABLE public.garantia (
    id integer NOT NULL,
    "fechaExpiracion" timestamp with time zone NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone,
    "idOrdenServicio" integer
);
CREATE SEQUENCE public.garantia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.garantia_id_seq OWNED BY public.garantia.id;
ALTER TABLE ONLY public.garantia ALTER COLUMN id SET DEFAULT nextval('public.garantia_id_seq'::regclass);
ALTER TABLE ONLY public.garantia
    ADD CONSTRAINT garantia_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.garantia
    ADD CONSTRAINT "garantia_idOrdenServicio_fkey" FOREIGN KEY ("idOrdenServicio") REFERENCES public."ordenServicio"(id) ON UPDATE CASCADE ON DELETE SET NULL;
    
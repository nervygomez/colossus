ALTER TABLE ONLY PUBLIC."solicitudServicio"
  DROP COLUMN IF EXISTS "motivoRechazo";
ALTER TABLE ONLY public."solicitudServicio"
  ADD COLUMN "idMotivoRechazo" integer REFERENCES PUBLIC."motivoRechazo"(id);
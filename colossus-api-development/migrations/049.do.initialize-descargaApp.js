const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

module.exports.generateSql = function () {
  return (
    `
    

    INSERT INTO "descargaApp" (titulo, descripcion, "urlImagen", "createdAt", "updatedAt") 
    values ('Descarga Nuestra Aplicacion', 'Por favor agregue una descripcion de la aplicacion, caracteristicas de la aplicacion de colossus',
      '${new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href}',
       NOW(), NOW()) ON CONFLICT (id) DO NOTHING;

    
    `
  )
}

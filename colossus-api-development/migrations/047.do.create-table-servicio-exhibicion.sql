CREATE TABLE public."servicioExhibicion" (
  id SERIAL PRIMARY KEY,
  descripcion character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone,
  "idCatalogoServicio" integer REFERENCES public."catalogoServicio"(id)
);

INSERT INTO "servicioExhibicion" (descripcion, "createdAt", "updatedAt") values ('primero', now(), now());
INSERT INTO "servicioExhibicion" (descripcion, "createdAt", "updatedAt") values ('segundo', now(), now());
INSERT INTO "servicioExhibicion" (descripcion, "createdAt", "updatedAt") values ('tercero', now(), now());
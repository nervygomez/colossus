const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaults = require('../config/path.json').defaults
const staticRoot = require('../config/path.json').staticRoot

const path = require('path')

module.exports.generateSql = function () {
  if (process.env.COLOSSUS_SEED !== 'true') { return ('') } // do nothing

  return (
    `
    INSERT INTO "catalogoServicio"  
    (descripcion, "idTipoEquipo" ,"idTipoServicio", "urlImagen", "createdAt", "updatedAt")
    select 'Reparación Smartphones', te.id, 1,
    '${new URL(path.join(staticRoot, defaults, 'telefonia.jpg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "tipoEquipo" te where nombre='Smartphones' ON CONFLICT (id) DO NOTHING;  

    INSERT INTO "catalogoServicio"  
    (descripcion, "idTipoEquipo" ,"idTipoServicio", "urlImagen", "createdAt", "updatedAt")
    select 'Reparación Laptops', te.id, 1,
    '${new URL(path.join(staticRoot, defaults, 'computacion.jpg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "tipoEquipo" te where nombre='Laptops' ON CONFLICT (id) DO NOTHING;  

    INSERT INTO "catalogoServicio"  
    ( descripcion, "idTipoEquipo" ,"idTipoServicio", "urlImagen", "createdAt", "updatedAt")
    select 'Reparación Consolas', te.id, 1,
    '${new URL(path.join(staticRoot, defaults, 'videojuegos.jpeg'), 'http://' + mainUrl + ':' + port).href}', NOW(), NOW()
    from "tipoEquipo" te where nombre='Consolas' ON CONFLICT (id) DO NOTHING;  
    `
  )
}

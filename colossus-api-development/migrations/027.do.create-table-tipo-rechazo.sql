CREATE TABLE PUBLIC."motivoRechazo" (
  id SERIAL PRIMARY KEY,
  tipoRechazo character varying(255),
  descripcion character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone
);
CREATE TABLE public."preguntaFrecuente" (
    id SERIAL PRIMARY KEY,
    "pregunta" character varying (500),
    "respuesta" character varying (2000),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "deletedAt" timestamp with time zone
);
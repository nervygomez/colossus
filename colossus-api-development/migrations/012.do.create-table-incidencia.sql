CREATE TABLE public.incidencia(
  id SERIAL PRIMARY KEY,
  descripcion character varying(255),
  "createdAt" timestamp with time zone NOT NULL,
  "updatedAt" timestamp with time zone NOT NULL,
  "deletedAt" timestamp with time zone,
  "idAgenda" integer REFERENCES PUBLIC.agenda(id),
  "idTipoIncidencia" integer REFERENCES PUBLIC."tipoIncidencia"(id)
);
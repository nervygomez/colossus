const path = require('path')

module.exports = {
  'migrationDirectory': path.join(__dirname, '../migrations'),
  'driver': 'pg',
  'host': process.env.host || '127.0.0.1',
  'port': 5432,
  'database': process.env.COLOSSUS_DEV_DATABASE || 'colossi_development',
  'username': process.env.COLOSSUS_DEV_USERNAME || 'postgres',
  'password': process.env.COLOSSUS_DEV_PASSWORD || 'postgres',
  validateChecksums: false
}

const OneSignal = require('onesignal-node')
const oneSignalclient = require('../config/oneSignalClient').oneSignalClient
const throwException = require('./helpers').throwException

module.exports = { sendMobileNotificationToSegments, sendMobileNotificationToSpecificUsers }

async function sendMobileNotificationToSegments (notification, includedSegments, excludedSegments = undefined) {
  // TODO: IMPLEMENT
  let notificationObj = new OneSignal.Notification({
    headings: { es: notification.titulo, en: notification.titulo },
    contents: { es: notification.descripcion, en: notification.descripcion },
    included_segments: includedSegments,
    excluded_segments: excludedSegments,
    content_available: true
  })
  console.log(notificationObj)

  oneSignalclient.sendNotification(notificationObj)
    .then((response) => {
      console.log(response.data, response.httpResponse.statusCode)
    })
    .catch((err) => {
      console.error(err)
      throwException('E-notificacion-01')
    })
}

async function sendMobileNotificationToSpecificUsers (notification, playerIds) {
  // TODO: IMPLEMENT
  let notificationObj = new OneSignal.Notification({
    headings: { es: notification.titulo, en: notification.titulo },
    contents: { es: notification.descripcion, en: notification.descripcion },
    include_player_ids: playerIds,
    big_picture: notification.urlImagen,
    content_available: true,
    data: {
      entidad: notification.entidad,
      idEndidad: notification.idEndidad
    }
  })
  console.log(notificationObj)

  oneSignalclient.sendNotification(notificationObj)
    .then((response) => {
      console.log(response.data, response.httpResponse.statusCode)
    })
    .catch((err) => {
      console.error(err)
      throwException('E-notificacion-01')
    })
}

'use strict'
const messages = require('./messages')

function throwException (codeMessage) {
  const e = { code: codeMessage, message: messages[codeMessage] }
  throw e
}

function isEmpty (obj) {
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false
    }
  }
  return true
}

module.exports = {
  throwException,
  isEmpty
}

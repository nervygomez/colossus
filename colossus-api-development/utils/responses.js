const makeTemplate = require('../templates')
const messages = require('./messages')

function makeResponseOk (res, data, pathTemplate, code = null) {
  if (code !== null) data.message = { code: code, text: messages[[code]] }
  const json = makeTemplate(data, pathTemplate)
  res.status(200).json(json)
}

function makeResponseOkHeader (res, data, pathTemplate, header) {
  res.set(header.key, header.value)
  const json = makeTemplate(data, pathTemplate)
  res.status(200).json(json)
}

function makeResponseOkMessage (res, code) {
  const message = { code: code, text: messages[[code]] }
  const json = makeTemplate({ message }, 'response/responseOkMessage')
  res.status(200).json(json)
}

function makeResponseException (res, error) {
  if (!error.hasOwnProperty('code')) { // If error has no code, i.e. it's a generic exception
    error.code = 'E000'
  }
  console.error(error)
  const json = makeTemplate({ error }, 'response/responseException')
  res.status(200).json(json)
  /*
      Status should be 400 (bad request), for some reason in Angular http
      provider receives the response body in other format in 200 and 400
  */
}

module.exports = {
  makeResponseException,
  makeResponseOkMessage,
  makeResponseOk,
  makeResponseOkHeader
}

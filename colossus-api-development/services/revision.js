'use strict'
module.exports = {
  createRevision,
  getRevision,
  getAllRevision,
  updateRevision
}

const Revision = require('../models').Revision
const SolicitudServicio = require('../models').SolicitudServicio

const sOrdenServicio = require('./ordenServicio')
const sBitacora = require('./bitacora')
// const sSolicitudServicio = require('./solicitudServicio')

// const sSolicitud = require('./solicitudServicio')
// const mSolicitud = require('../models').mSolicitud
// const sequelize = require('../models').sequelize
const throwException = require('../utils/helpers').throwException

async function createRevision (idSolicitud, transaction) {
  console.log(idSolicitud)
  await Revision.create(
    { idSolicitudServicio: idSolicitud },
    { transaction: transaction }
  )
}

// async function getRevisionByRechazo () {}

async function getRevision (idSolicitudServicio, transaction = undefined) {
  const revision = await Revision.findOne({
    where: { idSolicitudServicio: idSolicitudServicio },
    include: [SolicitudServicio]
  }, transaction)
  if (!revision) {
    throwException('E03301')
  }
  return revision
}

async function getAllRevision () {
  return Revision.findAll({
    include: [SolicitudServicio],
    order: [['id', 'asc']]
  })
}

async function updateRevision (idSolicitud, data, transaction) {
  console.log(data)
  if (!data.estatusEquipo) {
    throwException('E03304')
  }

  if (!data.costo) {
    throwException('E03305')
  }

  if (!data.diagnostico) {
    throwException('E03306')
  }

  // const solicitudServicio = sSolicitudServicio.getSolicitudServicio(idSolicitud)
  const revision = await getRevision(idSolicitud, transaction)

  if (revision.estatusEquipo !== 'E') {
    throwException('E03302')
  }

  if (data.estatusEquipo === 'R') { // no reparable
    if (!data.idMotivoRechazo) {
      throwException('E03320')
    }
    await SolicitudServicio.update(
      { estatus: 'R',
        idMotivoRechazo: data.idMotivoRechazo },
      { where: { id: idSolicitud }, transaction: transaction })
    const bitacoraObj = {
      idSolicitudServicio: idSolicitud,
      descripcion: 'El equipo no es reparable'
    }
    await sBitacora.createBitacora(bitacoraObj, transaction)
    await revision.update(data, { transaction })
    await sOrdenServicio.setAsDelivery(idSolicitud, transaction)
    return
  }

  if (data.estatusEquipo === 'A') { // reparable
    const bitacoraObj = {
      idSolicitudServicio: idSolicitud,
      descripcion: 'Equipo es reparable, pendiente presupuesto'
    }
    await sBitacora.createBitacora(bitacoraObj, transaction)
    return revision.update(data, { transaction })
  }

  throwException('E03303')
}

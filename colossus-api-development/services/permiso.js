'use strict'
const Permiso = require('../models').Permiso
const Rol = require('../models').Rol
const throwException = require('../utils/helpers').throwException

async function getAllPermiso () {
  const findall = await Permiso.findAll({
    order: [['id', 'asc']],
    include: [Rol]
  })
  return findall
}

async function getPermiso (id) {
  const permiso = await Permiso.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [Rol]
  })

  if (!permiso) {
    throwException('E00301')
  }
  console.log(permiso.get({ plain: true }))
  return permiso
}

module.exports = {
  getPermiso,
  getAllPermiso
}

'use strict'

module.exports = {
  scheduleRevision,
  scheduleActivity,
  getAgenda,
  getAllAgenda,
  updateAgenda,
  deleteAgenda,
  getAvailablesEmpleados,
  setAsCompleted,
  rescheduleAgenda,
  getAgendasForUser,
  getPendingAgendasForUser,
  getAgendaForReclamo
}

const Agenda = require('../models').Agenda
const Usuario = require('../models').Usuario
const Actividad = require('../models').Actividad
const Incidencia = require('../models').Incidencia
const ModeloEquipo = require('../models').ModeloEquipo
const Marca = require('../models').Marca

const CatalogoServicio = require('../models').CatalogoServicio
const OrdenServicio = require('../models').OrdenServicio
const SolicitudServicio = require('../models').SolicitudServicio
const TipoIncidencia = require('../models').TipoIncidencia
const sequelize = require('../models').sequelize
const Sequelize = require('../models').Sequelize
const Op = require('../models').Sequelize.Op

const BloqueHorario = require('../models').BloqueHorario
const sUsuario = require('./usuario')
const sBloqueHorario = require('./bloqueHorario')
const sCatalogoServicio = require('./catalogoServicio')
const sActividad = require('./actividad')
const sNotificacion = require('./notificacion')
const sSolicitudServicio = require('./solicitudServicio')
const sOrdenServicio = require('./ordenServicio')
const sIncidencia = require('./incidencia')
const throwException = require('../utils/helpers').throwException
const moment = require('moment')

async function validateData (data, transaction = null) {
  if (!data.idUsuario) {
    throwException('E-agenda-02')
  }
  await sUsuario.getEmpleado(data.idUsuario)

  if (!data.idBloqueHorario) {
    throwException('E-agenda-03')
  }
  await sBloqueHorario.getBloqueHorario(data.idBloqueHorario)

  if (!data.fechaActividad) {
    throwException('E-agenda-05')
  }

  await isAvailable(data.idUsuario, data.idBloqueHorario, data.fechaActividad, transaction)

  if (!data.idActividad) {
    throwException('E-agenda-06')
  }

  if (!data.idOrdenServicio) {
    throwException('E-agenda-07')
  }
}

async function scheduleRevision (data, transaction) {
  // await sOrdenServicio.getAgendableOrdenServicio(data.idOrdenServicio)
  // I needed to remove the validation on order reservation because it would try and find the order
  // before commiting the changes so it would say it could not find it, and end abruptly
  // without creating an event, therefore it could not create a solicitudServicio
  // yet, it is still needed to validate on actividad so I have to call it there
  await validateData(data)
  console.log(data)
  await sActividad.getActividadOrRevision(data.idActividad)
  return Agenda.create(data, { transaction: transaction })
}

async function scheduleActivity (data) {
  await validateData(data)
  data.estatus = 'E'
  const orden = await sOrdenServicio.getOrdenServicioExcludingRevisionActivities(data.idOrdenServicio)
  if (orden.estatusReclamo) {
    data.porReclamo = true
  } else {
    await sOrdenServicio.getAgendableOrdenServicio(data.idOrdenServicio)
  }

  const actividad = await sActividad.getActividad(data.idActividad)
  data.descripcion = actividad.nombre
  await notAlreadyScheduled(data.idOrdenServicio, data.idActividad, data.porReclamo)
  const agenda = await Agenda.create(data)
  if (!data.porReclamo) {
    await sOrdenServicio.verifyIfFullyScheduled(data.idOrdenServicio)
  } else {
    console.log(`actividad ${data.idActividad} por reclamo`)
  }
  return agenda
}

async function isAvailable (empleado, bloqueHorario, fecha, transaction = null) {
  const newfecha = moment(fecha, 'DD-MM-YYYY')
  // transforms from dd-mm-yyyy to yyyy-mm-dd
  const agenda = await Agenda.count({
    where: {
      idUsuario: empleado,
      idBloqueHorario: bloqueHorario,
      fechaActividad: newfecha,
      estatus: 'E'
    },
    transaction: transaction
  })
  if (agenda !== 0) {
    throwException('E-agenda-10')
  }
}

async function notAlreadyScheduled (ordenServicio, actividad, porReclamo = undefined) {
  if (await isScheduled(ordenServicio, actividad, porReclamo)) {
    throwException('E-agenda-09')
  }
}

async function hasToBeScheduled (ordenServicio, actividad, porReclamo = undefined) {
  if (await !isScheduled(ordenServicio, actividad, porReclamo)) {
    throwException('E-agenda-12')
  }
}

async function isScheduled (ordenServicio, actividad, porReclamo = undefined) {
  let conditions = {
    idOrdenServicio: ordenServicio,
    idActividad: actividad,
    estatus: {
      [Op.ne]: 'R'
    }
  }

  if (porReclamo) {
    conditions.porReclamo = true
  }

  const agenda = await Agenda.findOne({
    where: conditions
  })
  if (agenda === null) {
    return false
  }
  return true
}

async function getAvailablesEmpleados (data) {
  if (data.idBloqueHorario && data.fecha && data.idCatalogoServicio) {
    await sBloqueHorario.getBloqueHorario(data.idBloqueHorario)
    await sCatalogoServicio.getCatalogoServicio(data.idCatalogoServicio)
    data.fecha = moment(data.fecha, 'DD-MM-YYYY').format('YYYY-MM-DD')
  } else {
    throwException('E-agenda-011')
  }

  const empleado = await sequelize
    .query(` SELECT us."id", us."nombre", us."apellido", us."correo", us."direccion", us."telefono",
  us."fechaNacimiento", us."sexo", us."urlFoto", us."documentoIdentidad",
  us."createdAt", us."updatedAt", us."deletedAt", us."idRol" FROM usuario AS us 
  JOIN especialidad AS es ON us."id"=es."usuarioId"
  JOIN "catalogoServicio" AS cs ON cs."id"=es."catalogoServicioId"
  WHERE us.id NOT IN (SELECT "idUsuario" FROM agenda WHERE 
  "idBloqueHorario" = :bloqueHorario AND "fechaActividad" = :fecha
  AND "deletedAt" IS NULL AND estatus = 'E')
  AND cs."id" = :especialidad AND us."deletedAt" IS NULL 
  AND cs."deletedAt" IS NULL`,
    {
      model: Usuario,
      type: Sequelize.QueryTypes.SELECT,
      replacements: {
        bloqueHorario: data.idBloqueHorario,
        fecha: data.fecha,
        especialidad: data.idCatalogoServicio
      }
    })
  return empleado
}

async function getAgendaForReclamo (data) {
  const agenda = await Agenda.findOne({
    where: {
      idOrdenServicio: data.idOrdenServicio,
      porReclamo: data.porReclamo
    },
    include: [Usuario, BloqueHorario, {
      model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
      include: [CatalogoServicio]
    }, {
      model: OrdenServicio,
      include: [{
        model: SolicitudServicio,
        include: [Usuario] }]
    }, {
      model: Incidencia,
      include: [TipoIncidencia]
    }]
  })
  if (!agenda) {
    throwException('E-agenda-01')
  }
  return agenda
}
async function getAgenda (id, transaction) {
  const agenda = await Agenda.findOne({
    transaction: transaction,
    where: { id },
    include: [Usuario, BloqueHorario, {
      model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
      include: [CatalogoServicio]
    }, {
      model: OrdenServicio,
      include: [{
        model: SolicitudServicio,
        include: [Usuario] }]
    }, {
      model: Incidencia,
      include: [TipoIncidencia]
    }]
  })
  if (!agenda) {
    throwException('E-agenda-01')
  }
  return agenda
}

async function getAllAgenda (data) {
  // data should be:
  // idTecnico, idCliente, despuesDe, antesDe

  let conditions = {}

  if (data.idTecnico) {
    conditions.idUsuario = data.idTecnico
  }

  return Agenda.findAll({
    where: conditions,
    include: [Usuario, BloqueHorario, Incidencia, {
      model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
      include: [CatalogoServicio]
    }, {
      model: OrdenServicio,
      include: [Usuario]
    }],
    order: [['id', 'asc']]
  })
}

async function getAgendasForUser (currentUser) {
  return Agenda.findAll({
    include: [ BloqueHorario, Incidencia,
      {
        model: Usuario,
        where: { id: currentUser.id }
      }, {
        model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
        include: [CatalogoServicio]
      }, {
        model: OrdenServicio,
        include: [{
          model: SolicitudServicio,
          include: [Usuario] }]
      }],
    order: [['estatus', 'desc'], ['fechaActividad', 'asc'], [BloqueHorario, 'horaInicio', 'desc']]
  })
}

async function getPendingAgendasForUser (currentUser, query) {
  console.log(query)
  if (query.revision === 'true') {
    return Agenda.findAll({
      where: { estatus: 'E' },
      include: [ BloqueHorario, Incidencia,
        {
          model: Usuario,
          where: { id: currentUser.id }
        }, {
          model: Actividad.unscoped(), // unscoped() IS IMPORTANT, OTHERWISE IT WILL IGNORE REVISION ACTIVITIES
          include: [CatalogoServicio]
        }, {
          model: OrdenServicio,
          include: [Usuario, {
            model: SolicitudServicio,
            include: [Usuario, {
              model: ModeloEquipo,
              include: [Marca]
            }]
          }]
        }],
      order: [['estatus', 'desc'], ['fechaActividad', 'asc'], [BloqueHorario, 'horaInicio', 'desc']]
    })
  } else {
    return Agenda.findAll({
      where: { estatus: 'E' },
      include: [ BloqueHorario, Incidencia,
        {
          model: Usuario,
          where: { id: currentUser.id }
        }, {
          model: Actividad, // unscoped() SHOULD NOT BE HERE, AS I WANT TO IGNORE REVISION
          required: true,
          include: [CatalogoServicio]
        }, {
          model: OrdenServicio,
          include: [Usuario, {
            model: SolicitudServicio,
            include: [Usuario, {
              model: ModeloEquipo,
              include: [Marca]
            }]
          }]
        }],
      order: [['estatus', 'desc'], ['fechaActividad', 'asc'], [BloqueHorario, 'horaInicio', 'desc']]
    })
  }
}

async function updateAgenda (id, data) {
  const agenda = await getAgenda(id)
  if (!agenda) {
    throwException('E-agenda-01')
  }
  if (data.horaInicio > data.horaFinal) {
    throwException('E-agenda-02')
  }

  return agenda.update(data)
}

async function deleteAgenda (id, transaction = undefined) {
  await getAgenda(id)
  return Agenda.destroy({
    where: { id }
  }, { transaction })
}

async function setAsCompleted (id) {
  const agenda = await getAgenda(id)
  if (agenda.estatus !== 'E') {
    throwException('E-agenda-08')
  }
  const updatedAgenda = await agenda.update({ estatus: 'L' })
  // await sOrdenServicio.verifyIfFullyCompleted(agenda.idOrdenServicio)
  // removed because it is required to be manually initiated by the user, not automatically
  return updatedAgenda
}

async function rescheduleAgenda (id, data) {
  // data has a new date and hour, and a ref to the previous agenda
  // data also has a ref to the incidence type, and a decription

  let agenda = await getAgenda(id)
  if (agenda.estatus !== 'E') {
    throwException('E-agenda-12')
  }

  if (!data.idTipoIncidencia) {
    throwException('E-agenda-13')
  }

  if (!data.idBloqueHorario) {
    throwException('E-agenda-03')
  }

  if (!data.fechaActividad) {
    throwException('E-agenda-05')
  }

  const actividad = await sActividad.getActividadOrRevision(agenda.idActividad)
  await hasToBeScheduled(agenda.idOrdenServicio, actividad.id) // needs to be verified before rescheduling

  const newAgenda = await sequelize.transaction(async transaction => {
    await agenda.update({ estatus: 'R' }, { transaction: transaction })
    const newAgendaObj = {
      idUsuario: agenda.idUsuario,
      descripcion: agenda.descripcion,
      idBloqueHorario: data.idBloqueHorario, // new
      fechaActividad: data.fechaActividad, // new
      idActividad: agenda.idActividad,
      idOrdenServicio: agenda.idOrdenServicio
    }
    await validateData(newAgendaObj, transaction)

    const newAgenda = await Agenda.create(newAgendaObj, { transaction: transaction })
    /*
    if (actividad.tipo === 'actividad') {
      await sOrdenServicio.verifyIfFullyScheduled(data.idOrdenServicio)
    }
    */
    // it probably is not necessary, but will keep it commented just in case
    console.log(newAgenda)
    await sIncidencia.createIncidencia({
      descripcion: data.descripcionIncidencia,
      idTipoIncidencia: data.idTipoIncidencia,
      idAgenda: agenda.id
    }, transaction)

    return newAgenda
  })
  const agendaVieja = await getAgenda(agenda.id)
  const agendaNueva = await getAgenda(newAgenda.id)

  console.log(agendaVieja)
  const solicitudServicio = await sSolicitudServicio.getSolicitudServicio(agenda.ordenServicio.solicitudServicio.id)
  const usuario = await sUsuario.getCliente(solicitudServicio.usuario.id)
  await sNotificacion.createIncidenceNotification(agendaNueva, agendaVieja, usuario, solicitudServicio, agenda.id)
}

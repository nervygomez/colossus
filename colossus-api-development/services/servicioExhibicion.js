'use strict'

const moment = require('moment')
const Op = require('../models').Sequelize.Op

const ServicioExhibicion = require('../models').ServicioExhibicion
const CatalogoServicio = require('../models').CatalogoServicio
const TipoServicio = require('../models').TipoServicio
const Actividad = require('../models').Actividad
const Promocion = require('../models').Promocion
const Descuento = require('../models').Descuento

const TipoEquipo = require('../models').TipoEquipo
const CategoriaTipoEquipo = require('../models').CategoriaTipoEquipo
const throwException = require('../utils/helpers').throwException

async function getAllServicioExhibicion () {
  const fActual = moment().format('YYYY-MM-DD')
  const servicioExhibicion = await ServicioExhibicion.findAll({
    include: [{
      model: CatalogoServicio,
      include: [TipoServicio, Actividad, {
        model: TipoEquipo,
        include: [CategoriaTipoEquipo]
      },
      {
        model: Promocion,
        required: false,
        include: Descuento,
        where: {
          fechaInicio: { [Op.lte]: fActual },
          fechaExpiracion: { [Op.gte]: fActual }
        }
      }]
    }],
    order: [['id', 'asc']]
  })
  if (!servicioExhibicion) {
    throwException('E-servicioExhibicion-01')
  }
  return servicioExhibicion
}

async function getServicioExhibicion (id) {
  const servicioExhibicion = await ServicioExhibicion.findOne({
    where: { id }
  })
  if (!servicioExhibicion) {
    throwException('E-servicioExhibicion-02')
  }
  return servicioExhibicion
}

async function updateServicioExhibicion (id, data) {
  await getServicioExhibicion(id)
  const servicioExhibicion = await ServicioExhibicion.findOne({
    where: { id }
  })
  return servicioExhibicion.update(data)
}

module.exports = {
  getAllServicioExhibicion,
  updateServicioExhibicion
}

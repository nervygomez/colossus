'use strict'

const TipoReclamo = require('../models').TipoReclamo
// const ReclamoServicio = require('../models').ReclamoServicio
const throwException = require('../utils/helpers').throwException

async function createTipoReclamo (data) {
  console.log(data)
  await TipoReclamo.create(data)
}

async function getTipoReclamo (id) {
  const tipoReclamo = await TipoReclamo.findOne({
    where: { id }
    // include: [ReclamoServicio]
  })
  if (!tipoReclamo) {
    throwException('E01501')
  }
  return tipoReclamo
}

async function getAllTipoReclamo () {
  return TipoReclamo.findAll({
    order: [['id', 'asc']]
    // include: [ReclamoSevicio]

  })
}

async function updateTipoReclamo (id, data) {
  let tipoReclamo = await getTipoReclamo(id)
  if (!tipoReclamo) {
    throwException('E01501')
  }
  return tipoReclamo.update(data)
}

async function deleteTipoReclamo (id) {
  await getTipoReclamo(id)
  return TipoReclamo.destroy({
    where: {
      id
    }
  })
}

module.exports = { createTipoReclamo,
  getTipoReclamo,
  getAllTipoReclamo,
  updateTipoReclamo,
  deleteTipoReclamo }

'use strict'
const Condicion = require('../models').Condicion
const throwException = require('../utils/helpers').throwException

async function createCondicion (data) {
  await Condicion.create(data)
}

async function getCondicion (id) {
  const condicion = await Condicion.findOne({
    where: { id }
  })
  if (!condicion) {
    throwException('E-condicion-01')
  }
  return condicion
}

async function getAllCondicion () {
  return Condicion.findAll({
    order: [['id', 'asc']]
  })
}
async function updateCondicion (id, data) {
  const condicion = await getCondicion(id)
  return condicion.update(data)
}
async function deleteCondicion (id) {
  await getCondicion(id)
  return Condicion.destroy({
    where: { id }
  })
}

module.exports = { createCondicion,
  getCondicion,
  getAllCondicion,
  updateCondicion,
  deleteCondicion
}

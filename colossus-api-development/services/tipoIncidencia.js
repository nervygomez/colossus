'use strict'

const TipoIncidencia = require('../models').TipoIncidencia
const throwException = require('../utils/helpers').throwException

async function createTipoIncidencia (data) {
  console.log(data)
  await TipoIncidencia.create(data)
}

async function getTipoIncidencia (id) {
  const tipoIncidencia = await TipoIncidencia.findOne({
    where: { id }
    // include: [Incidencia]
  })
  if (!tipoIncidencia) {
    throwException('E01401')
  }
  return tipoIncidencia
}

async function getAllTipoIncidencia () {
  return TipoIncidencia.findAll({
    order: [['id', 'asc']]
    // include: [Incidencia]
  })
}

async function updateTipoIncidencia (id, data) {
  const tipoIncidencia = await getTipoIncidencia(id)
  if (!tipoIncidencia) {
    throwException('E01401')
  }
  return tipoIncidencia.update(data)
}

async function deleteTipoIncidencia (id) {
  await getTipoIncidencia(id)
  return TipoIncidencia.destroy({
    where: { id }
  })
}

module.exports = { createTipoIncidencia,
  getTipoIncidencia,
  getAllTipoIncidencia,
  updateTipoIncidencia,
  deleteTipoIncidencia }

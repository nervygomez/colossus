'use stric'
const moment = require('moment')
const Usuario = require('../models').Usuario
const Rol = require('../models').Rol
const Agenda = require('../models').Agenda
const Incidencia = require('../models').Incidencia
const TipoIncidencia = require('../models').TipoIncidencia
const Marca = require('../models').Marca
const ModeloEquipo = require('../models').ModeloEquipo
const CatalogoServicio = require('../models').CatalogoServicio
const SolicitudServicio = require('../models').SolicitudServicio
const TipoEquipo = require('../models').TipoEquipo
const TipoServicio = require('../models').TipoServicio
const OrdenServicio = require('../models').OrdenServicio
const Garantia = require('../models').Garantia
const CategoriaTipoEquipo = require('../models').CategoriaTipoEquipo
const Revision = require('../models').Revision
const throwException = require('../utils/helpers').throwException
const Op = require('../models').Sequelize.Op
const sequelize = require('../models').sequelize
const CaracteristicaCliente = require('../models').CaracteristicaCliente
const TipoCaracteristicaCliente = require('../models').TipoCaracteristicaCliente
const momentDurationFormatSetup = require('moment-duration-format')
async function reporteSolicitud (data) {
  let cond = {}
  let condTipoE = {}
  let condCat = {}

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isBefore(fechaF)) {
      cond.createdAt = {
        [Op.lte]: fechaF,
        [Op.gte]: fechaI
      }
    } else {
      throwException('E-Reporte-01')
    }
  }

  if (data.estatus) {
    cond.estatus = data.estatus
  }

  if (data.idCategoriaTipoEquipo) {
    condCat.id = data.idCategoriaTipoEquipo
  }
  if (data.idTipoEquipo) {
    condTipoE.id = data.idTipoEquipo
  }
  return SolicitudServicio.findAll({
    where: cond,
    include: [{
      model: Usuario,
      include: [{
        model: Rol,
        where: { nombre: 'Cliente' }
      }]
    },
    {
      model: ModeloEquipo,
      include: [Marca]

    },
    {
      model: CatalogoServicio,
      required: true,
      include: [{
        model: TipoEquipo,
        where: condTipoE,
        include: [{
          model: CategoriaTipoEquipo,
          where: condCat
        }]
      }]
    }],
    order: [['id', 'asc']]
  })
}

async function reporteCliente (data) {
  let condCli = {}
  let condTipoCarac
  let condCarac
  let req = false

  if (data.idCaracteristicaCliente) {
    condCarac = { id: data.idCaracteristicaCliente }
    req = true
  }

  if (data.idTipoCaracteristicaCliente) {
    condTipoCarac = { id: data.idTipoCaracteristicaCliente }
    req = true
  }

  if (data.sexo) {
    condCli.sexo = data.sexo
  }
  if (data.edad) {
    const ano = parseInt(data.edad)
    const fechaSup = moment().subtract(ano, 'years').format('YYYY/MM/DD')
    let fechaM = moment().subtract(ano + 1, 'years')
    const fechaInf = fechaM.add(1, 'days').format('YYYY/MM/DD')
    condCli.fechaNacimiento = {
      [Op.lte]: fechaSup,
      [Op.gte]: fechaInf
    }
    console.log('fechas')
    console.log(fechaSup)
    console.log(fechaInf)
    console.log('++++++++++')
  }

  const usuario = await Usuario.findAll({
    where: condCli,
    order: [['fechaNacimiento', 'desc']],
    include: [{
      model: CaracteristicaCliente,
      required: req,
      where: condCarac,
      include: [{
        model: TipoCaracteristicaCliente,
        where: condTipoCarac
      }]
    },
    {
      model: Rol,
      where: { nombre: 'Cliente' }
    }]
  })

  console.log(usuario)
  return usuario
}

async function reporteGarantia (data) {
  let condGaran = {}
  let condTipoE = {}
  let condCat = {}

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isBefore(fechaF)) {
      condGaran.createdAt = {
        [Op.lte]: fechaF,
        [Op.gte]: fechaI
      }
    } else {
      throwException('E-Reporte-01')
    }
  }
  if (data.estatus === 'Activa') {
    condGaran.fechaExpiracion = {
      [Op.gte]: moment().startOf('day')
    }
  }
  if (data.estatus === 'Vencida') {
    condGaran.fechaExpiracion = {
      [Op.lt]: moment().endOf('day')
    }
  }
  if (data.idCategoriaTipoEquipo) {
    condCat.id = data.idCategoriaTipoEquipo
  }
  if (data.idTipoEquipo) {
    condTipoE.id = data.idTipoEquipo
  }
  return SolicitudServicio.findAll({
    include: [{
      model: Usuario,
      include: [{
        model: Rol,
        where: { nombre: 'Cliente' }
      }]
    },
    {
      model: ModeloEquipo,
      include: [Marca]

    },
    {
      model: OrdenServicio,
      required: true,
      include: [{
        model: Garantia,
        where: condGaran
      }]
    }, {
      model: CatalogoServicio,
      required: true,
      include: [{
        model: TipoEquipo,
        where: condTipoE,
        include: [{
          model: CategoriaTipoEquipo,
          where: condCat
        }]
      }]
    }],
    order: [['id', 'asc']]
  })
}

async function reporteRevision (data) {
  let condRev = {}
  let condTipoE = {}
  let condCat = {}
  let condSol = {}

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isBefore(fechaF)) {
      condSol.fechaRevisado = {
        [Op.lte]: fechaF,
        [Op.gte]: fechaI
      }
    } else {
      throwException('E-Reporte-01')
    }
  }
  if (data.estatusEquipo === 'E') {
    condRev.estatusEquipo = data.estatusEquipo
  }
  if (data.estatusEquipo === 'A') {
    condRev.estatusEquipo = data.estatusEquipo
  }
  if (data.estatusEquipo === 'R') {
    condRev.estatusEquipo = data.estatusEquipo
  }
  if (data.idCategoriaTipoEquipo) {
    condCat.id = data.idCategoriaTipoEquipo
  }
  if (data.idTipoEquipo) {
    condTipoE.id = data.idTipoEquipo
  }
  return SolicitudServicio.findAll({
    where: condSol,
    include: [{
      model: Usuario,
      include: [{
        model: Rol,
        where: { nombre: 'Cliente' }
      }]
    },
    {
      model: ModeloEquipo,
      include: [Marca]

    },
    {
      model: Revision,
      where: condRev
    },
    {
      model: CatalogoServicio,
      required: true,
      include: [{
        model: TipoEquipo,
        where: condTipoE,
        include: [{
          model: CategoriaTipoEquipo,
          where: condCat
        }]
      }]
    }],
    order: [['id', 'asc']]
  })
}

async function reporteIncidencia (data) {
  let condInc = {}
  let condTec = {}
  let condCat = {}
  let condTipoE = {}

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isBefore(fechaF)) {
      condInc.createdAt = {
        [Op.lte]: fechaF,
        [Op.gte]: fechaI
      }
    } else {
      throwException('E-Reporte-01')
    }
  }
  if (data.idTipoIncidencia) {
    condInc.idTipoIncidencia = data.idTipoIncidencia
  }
  if (data.idUsuario) {
    condTec.idUsuario = data.idUsuario
  }
  if (data.idCategoriaTipoEquipo) {
    condCat.id = data.idCategoriaTipoEquipo
  }
  if (data.idTipoEquipo) {
    condTipoE.id = data.idTipoEquipo
  }
  const inci = await SolicitudServicio.findAll({
    include: [
      {
        model: CatalogoServicio,
        required: true,
        include: [{
          model: TipoEquipo,
          where: condTipoE,
          include: [{
            model: CategoriaTipoEquipo,
            where: condCat
          }]
        }]
      },
      {
        model: OrdenServicio,
        where: condTec,
        include: [Usuario, {
          model: Agenda,
          required: true,
          include: [{
            model: Incidencia,
            where: condInc,
            required: true,
            include: [TipoIncidencia]
          }]
        }]
      },
      {
        model: ModeloEquipo,
        include: [Marca]
      }, Usuario]
  })

  console.log(inci)
  return inci
}

async function reporteEstadisticoSolicitud (data) {
  let condSol = {}
  let condTip = {}
  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isBefore(fechaF)) {
      condSol.createdAt = {
        [Op.lte]: fechaF,
        [Op.gte]: fechaI
      }
    } else {
      throwException('E-Reporte-01')
    }
  }
  if (data.idTipoServicio) {
    condTip.id = data.idTipoServicio
  }

  const count = await CatalogoServicio.findAll({
    attributes: { include: [[sequelize.fn('COUNT', sequelize.col('solicitudesServicio.idCatalogoServicio')), 'solicitudes']] },
    include: [{
      model: SolicitudServicio,
      attributes: [],
      where: condSol
    }, {
      model: TipoServicio,
      attributes: [],
      where: condTip
    }],
    group: ['CatalogoServicio.id']
  })

  return count
}

async function reporteEstadisticoTecnico (data) {
  let fechaI1 = moment(data.fechaDesde, 'DD-MM-YYYY').format('YYYY-MM-DD')
  let fechaF1 = moment(data.fechaHasta, 'DD-MM-YYYY').format('YYYY-MM-DD')

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isAfter(fechaF)) {
      throwException('E-Reporte-01')
    }
  }

  const reporte = await sequelize
    .query(`select us.id, us.nombre, us.apellido, count(ord.id) as "cantidadTrabajo", cateq.nombre as "nombreCat", 
    cateq.id as "idCategoriaTipoEquipo" from usuario AS us Inner Join "ordenServicio" AS ord 
    On us.id= ord."idUsuario" Inner Join "solicitudServicio" AS sol On ord."idSolicitudServicio" = sol.id 
    Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio" = cat.id Inner Join "tipoEquipo" AS eq 
    On cat."idTipoEquipo" = eq.id Inner Join "categoriaTipoEquipo" AS cateq On eq."idCategoriaTipoEquipo" = cateq.id 
    where ord."fechaFinalizacion" BETWEEN :fecha1 and :fecha2 group by us.id, cateq.id order by us.id`,
    {
      replacements: {
        fecha1: fechaI1,
        fecha2: fechaF1
      },
      raw: true
    })

  const tecnicos = reporte[0]
  let tech = []

  tecnicos.forEach(t => {
    let existing = tech.filter((v) => v.id === t.id)
    if (existing.length) {
      let existingIndex = tech.indexOf(existing[0])
      const objeto = {
        idCategoriaTipoEquipo: t.idCategoriaTipoEquipo,
        nombreCategoria: t.nombreCat,
        cantidadTrabajo: t.cantidadTrabajo
      }
      tech[existingIndex].trabajos = tech[existingIndex].trabajos.concat(objeto)
    } else {
      let tec = {
        id: t.id,
        nombre: t.nombre,
        apellido: t.apellido
      }
      const objeto = {
        idCategoriaTipoEquipo: t.idCategoriaTipoEquipo,
        nombreCategoria: t.nombreCat,
        cantidadTrabajo: t.cantidadTrabajo
      }
      tec.trabajos = [objeto]
      tech.push(tec)
    }
  })
  console.log(tech)

  return tech
}

async function reporteEstadisticoTecnico2 (data) {
  let fechaI1 = moment(data.fechaDesde, 'DD-MM-YYYY').format('YYYY-MM-DD')
  let fechaF1 = moment(data.fechaHasta, 'DD-MM-YYYY').format('YYYY-MM-DD')

  if (data.fechaDesde && data.fechaHasta) {
    let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
    let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY').endOf('day')
    if (fechaI.isAfter(fechaF)) {
      throwException('E-Reporte-01')
    }
  }

  const reporte = await sequelize
    .query(`select us.id, us.nombre, us.apellido, count(ord.id) as "cantidadTrabajo", cateq.nombre as "nombreCat", 
    cateq.id as "idCategoriaTipoEquipo" from usuario AS us Inner Join "ordenServicio" AS ord 
    On us.id= ord."idUsuario" Inner Join "solicitudServicio" AS sol On ord."idSolicitudServicio" = sol.id 
    Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio" = cat.id Inner Join "tipoEquipo" AS eq 
    On cat."idTipoEquipo" = eq.id Inner Join "categoriaTipoEquipo" AS cateq On eq."idCategoriaTipoEquipo" = cateq.id 
    where ord."fechaFinalizacion" BETWEEN :fecha1 and :fecha2 group by us.id, cateq.id order by "idCategoriaTipoEquipo"`,
    {
      replacements: {
        fecha1: fechaI1,
        fecha2: fechaF1
      },
      raw: true
    })
  const cat = reporte[0]
  let tech = []

  cat.forEach(t => {
    let existing = tech.filter((v) => v.idCategoriaTipoEquipo === t.idCategoriaTipoEquipo)
    if (existing.length) {
      let existingIndex = tech.indexOf(existing[0])
      const objeto = {
        id: t.id,
        nombre: t.nombre,
        apellido: t.apellido,
        cantidadTrabajo: t.cantidadTrabajo
      }
      tech[existingIndex].tecnicos = tech[existingIndex].tecnicos.concat(objeto)
    } else {
      let cate = {
        idCategoriaTipoEquipo: t.idCategoriaTipoEquipo,
        nombreCategoria: t.nombreCat
      }
      const objeto = {
        id: t.id,
        nombre: t.nombre,
        apellido: t.apellido,
        cantidadTrabajo: t.cantidadTrabajo
      }
      cate.tecnicos = [objeto]
      tech.push(cate)
    }
  })
  console.log(tech)

  return tech
}

async function reporteEstadisticoAnual (data) {
  const fechaI = `${data.ano}-01-01`
  const fechaF = `${data.ano}-12-31`

  const reporte = await sequelize
    .query(`select Extract( month from ord."fechaFinalizacion") as mes, cateq.nombre, 
    count(ord.id) as "contado" FROM "ordenServicio" AS ord INNER JOIN "solicitudServicio" 
    AS sol ON ord."idSolicitudServicio" = sol.id INNER JOIN "catalogoServicio" AS cat 
    On sol."idCatalogoServicio" = cat.id Inner Join "tipoEquipo" AS eq On cat."idTipoEquipo" = eq.id 
    Inner Join "categoriaTipoEquipo" AS cateq On eq."idCategoriaTipoEquipo" = cateq.id where 
    ord."fechaFinalizacion" IS NOT null and ord."fechaFinalizacion" BETWEEN :fecha1 AND :fecha2 
    GROUP BY cateq.id, mes order by mes`,
    {
      replacements: {
        fecha1: fechaI,
        fecha2: fechaF
      },
      raw: true
    })
  const servicios = reporte[0]

  let mo = []

  servicios.forEach(s => {
    let existing = mo.filter((v) => v.mes === s.mes)
    if (existing.length) {
      let existingIndex = mo.indexOf(existing[0])
      const objeto = {
        nombre: s.nombre,
        cantidad: s.contado
      }
      mo[existingIndex].categorias = mo[existingIndex].categorias.concat(objeto)
      console.log(existing)
    } else {
      let month = {
        mes: s.mes
      }
      const objeto = {
        nombre: s.nombre,
        cantidad: s.contado
      }
      month.categorias = [objeto]
      mo.push(month)
    }
  })
  console.log(mo)
  return mo
}

async function reporteEstadisticoTiempoProm (data) {
  if (!data.fechaDesde && !data.fechaHasta) {
    throwException('E-Reporte-02')
  }
  let fechaI = moment(data.fechaDesde, 'DD-MM-YYYY')
  let fechaF = moment(data.fechaHasta, 'DD-MM-YYYY')

  if (fechaI.isAfter(fechaF)) {
    throwException('E-Reporte-01')
  }

  fechaF = fechaF.format('YYYY-MM-DD')
  fechaI = fechaI.format('YYYY-MM-DD')
  let queryStrSol
  let queryStrRev
  let queryStrRep
  let replacements = {
    fecha1: fechaI,
    fecha2: fechaF
  }

  if (data.idCategoriaTipoEquipo) {
    replacements.cate = data.idCategoriaTipoEquipo
    queryStrSol = `select date_part('epoch',avg(age(sol."fechaRespuesta", sol."createdAt"))) as "duracionRespuesta" 
    from "solicitudServicio" sol Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio"
     = cat.id Inner Join "tipoEquipo" AS teq On cat."idTipoEquipo" = teq.id Inner Join
      "categoriaTipoEquipo" AS cateq On teq."idCategoriaTipoEquipo" = cateq.id Inner Join 
      revision as rev On rev."idSolicitudServicio" = sol.id where sol."createdAt" 
      >= :fecha1 and sol."fechaRespuesta"<= :fecha2 and cateq.id = :cate`
    queryStrRev = `select date_part('epoch',avg(age(sol."fechaRevisado",sol."fechaRespuesta"))) as "duracionRevision"
     from "solicitudServicio" sol Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio" = cat.id Inner
      Join "tipoEquipo" AS teq On cat."idTipoEquipo" = teq.id Inner Join "categoriaTipoEquipo" AS cateq On teq.
      "idCategoriaTipoEquipo" = cateq.id Inner Join  revision as rev On rev."idSolicitudServicio" = sol.id where 
      sol."createdAt"  >= :fecha1 and sol."fechaRespuesta"<= :fecha2 and cateq.id = :cate`
    queryStrRep = `select date_part('epoch',avg(age(o."fechaFinalizacion",o."fechaRespuestaCliente"))) as "duracionReparacion" 
    from "ordenServicio" o Inner Join "solicitudServicio" sol ON o."idSolicitudServicio" = sol.id INNER JOIN "catalogoServicio" 
    cat On sol."idCatalogoServicio" = cat.id Inner Join "tipoEquipo"  teq On cat."idTipoEquipo" = teq.id Inner Join 
    "categoriaTipoEquipo"  cateq On teq."idCategoriaTipoEquipo" = cateq.id where o."fechaRespuestaCliente" >= :fecha1 and 
    o."fechaFinalizacion"<= :fecha2 and cateq.id = :cate`
  } else {
    queryStrSol = `select date_part('epoch',avg(age(sol."fechaRespuesta", sol."createdAt"))) as "duracionRespuesta" 
    from "solicitudServicio" sol Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio"
     = cat.id Inner Join "tipoEquipo" AS teq On cat."idTipoEquipo" = teq.id Inner Join
      "categoriaTipoEquipo" AS cateq On teq."idCategoriaTipoEquipo" = cateq.id Inner Join 
      revision as rev On rev."idSolicitudServicio" = sol.id where sol."createdAt" 
      >= :fecha1 and sol."fechaRespuesta"<= :fecha2`
    queryStrRev = `select date_part('epoch',avg(age(sol."fechaRevisado",sol."fechaRespuesta"))) as "duracionRevision"
      from "solicitudServicio" sol Inner Join "catalogoServicio" AS cat On sol."idCatalogoServicio" = cat.id Inner
       Join "tipoEquipo" AS teq On cat."idTipoEquipo" = teq.id Inner Join "categoriaTipoEquipo" AS cateq On teq.
       "idCategoriaTipoEquipo" = cateq.id Inner Join  revision as rev On rev."idSolicitudServicio" = sol.id where 
       sol."createdAt"  >= :fecha1 and sol."fechaRespuesta"<= :fecha2`
    queryStrRep = `select date_part('epoch',avg(age(o."fechaFinalizacion",o."fechaRespuestaCliente"))) as "duracionReparacion" 
       from "ordenServicio" o Inner Join "solicitudServicio" sol ON o."idSolicitudServicio" = sol.id INNER JOIN "catalogoServicio" 
       cat On sol."idCatalogoServicio" = cat.id Inner Join "tipoEquipo"  teq On cat."idTipoEquipo" = teq.id Inner Join 
       "categoriaTipoEquipo"  cateq On teq."idCategoriaTipoEquipo" = cateq.id where o."fechaRespuestaCliente" >= :fecha1 and 
       o."fechaFinalizacion"<= :fecha2`
  }

  const reporteSol = await sequelize
    .query(queryStrSol,
      {
        replacements,
        raw: true
      })

  const reporteRev = await sequelize
    .query(queryStrRev,
      {
        replacements,
        raw: true
      })

  const reporteRep = await sequelize
    .query(queryStrRep,
      {
        replacements,
        raw: true
      })
  const segundosSol = reporteSol[0][0].duracionRespuesta
  const segundosRev = reporteRev[0][0].duracionRevision
  const segundosRep = reporteRep[0][0].duracionReparacion

  const soli = moment.duration(segundosSol, 'seconds').format('d [dias] hh:mm:ss')
  const revi = moment.duration(segundosRev, 'seconds').format('d [dias] hh:mm:ss')
  const repa = moment.duration(segundosRep, 'seconds').format('d [dias] hh:mm:ss')
  const promedio = { promSolicitud: soli, segundosSol, promRevision: revi, segundosRev, promReparacion: repa, segundosRep }
  console.log(promedio)
  return promedio
}

module.exports = {
  reporteSolicitud,
  reporteGarantia,
  reporteEstadisticoSolicitud,
  reporteRevision,
  reporteIncidencia,
  reporteEstadisticoTecnico,
  reporteEstadisticoTecnico2,
  reporteEstadisticoAnual,
  reporteCliente,
  reporteEstadisticoTiempoProm
}

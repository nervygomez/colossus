'use strict'

const CategoriaTipoEquipo = require('../models').CategoriaTipoEquipo
// const sequelize = require('../models').sequelize
const TipoEquipo = require('../models').TipoEquipo
const throwException = require('../utils/helpers').throwException
// const handleFile = require('../utils/fileHandler').handleFile
// const messages = require('../utils/messages')

async function getCategoriaTipoEquipo (id) {
  const categoriaTipoEquipo = await CategoriaTipoEquipo.findOne({
    where: { id },
    include: [TipoEquipo],
    order: [['id', 'asc']]
  })
  if (!categoriaTipoEquipo) {
    throwException('E00201')
  }
  return categoriaTipoEquipo
}

async function createCategoriaTipoEquipo (data) {
  console.log(data)
  return CategoriaTipoEquipo.create(data)
}

async function getAllCategoriaTipoEquipo () {
  return CategoriaTipoEquipo.findAll({
    include: [TipoEquipo],
    order: [['id', 'asc']]
  })
}

async function updateCategoriaTipoEquipo (id, data) {
  const categoriaTipoEquipo = await getCategoriaTipoEquipo(id)

  if (!categoriaTipoEquipo) {
    throwException('E00201')
  }

  return categoriaTipoEquipo.update(data)
}

async function deleteCategoriaTipoEquipo (id) {
  await getCategoriaTipoEquipo(id)
  return CategoriaTipoEquipo.destroy({
    where: {
      id: id
    }
  })
}

module.exports = {
  getCategoriaTipoEquipo,
  createCategoriaTipoEquipo,
  getAllCategoriaTipoEquipo,
  updateCategoriaTipoEquipo,
  deleteCategoriaTipoEquipo
}

'use strict'

const moment = require('moment')
const Op = require('../models').Sequelize.Op

const Promocion = require('../models').Promocion
const Descuento = require('../models').Descuento
const CatalogoServicio = require('../models').CatalogoServicio
const TipoServicio = require('../models').TipoServicio
const TipoEquipo = require('../models').TipoEquipo

const sDescuento = require('../services/descuento')
const sCatalogoServicio = require('../services/catalogoServicio')
const sNotificacion = require('../services/notificacion')
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderImage = require('../config/path.json').defaultPlaceholderImage

async function createPromocion (data, img) {
  if (!data.idDescuento) {
    throwException('E-promocion-03')
  }

  if (!data.idCatalogoServicio) {
    throwException('E-promocion-04')
  }

  const fActual = moment().startOf('day')
  const fInicio = moment(data.fechaInicio, 'DD-MM-YYYY')
  const fExpiracion = moment(data.fechaExpiracion, 'DD-MM-YYYY')

  console.log(fActual, fInicio)

  if (fInicio.isBefore(fActual)) {
    throwException('E-promocion-06')
  }

  if (fInicio.isAfter(fExpiracion)) {
    throwException('E-promocion-07')
  }

  await sDescuento.getDescuento(data.idDescuento)
  await sCatalogoServicio.getCatalogoServicio(data.idCatalogoServicio)
  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'promocion')
    } else {
      data.urlImagen = new URL(defaultPlaceholderImage, 'http://' + mainUrl + ':' + port).href
    }
    await Promocion.create(data, { t })
  })
}

async function getAllPromocionVigente () {
  const fActual = moment().startOf('day')
  const promocion = await Promocion.findAll({
    where: {
      fechaInicio: { [Op.lte]: fActual },
      fechaExpiracion: { [Op.gte]: fActual }
    },
    include: [Descuento,
      {
        model: CatalogoServicio,
        include: [TipoEquipo, TipoServicio]
      }],
    order: [['id', 'asc']]
  })

  if (!promocion) {
    throwException('E-promocion-01')
  }
  return promocion
}

async function getAllPromocion (data) {
  const promocion = await Promocion.findAll({
    include: [Descuento,
      {
        model: CatalogoServicio,
        include: [TipoEquipo, TipoServicio]
      }],
    order: [['id', 'asc']]
  })

  if (!promocion) {
    throwException('E-promocion-01')
  }
  return promocion
}

async function getPromocionByServicio (idServicio) {
  const fActual = moment().startOf('day')
  const promocion = await Promocion.findAll({
    where: {
      idCatalogoServicio: idServicio,
      fechaInicio: { [Op.lte]: fActual },
      fechaExpiracion: { [Op.gte]: fActual }
    },
    include: [Descuento,
      {
        model: CatalogoServicio,
        include: [TipoEquipo, TipoServicio]
      }],
    order: [['id', 'asc']]
  })
  if (!promocion) {
    throwException('E-promocion-05')
  }
  return promocion
}

async function getPromocion (id) {
  const promocion = await Promocion.findOne({
    where: { id },
    include: [Descuento,
      {
        model: CatalogoServicio,
        include: [TipoEquipo, TipoServicio]
      }],
    order: [['id', 'asc']]
  })
  if (!promocion) {
    throwException('E-promocion-02')
  }
  return promocion
}

async function getPromocionVigente (id) {
  const fActual = moment().startOf('day')
  const promocion = await Promocion.findOne({
    where: { id,
      fechaInicio: { [Op.lte]: fActual },
      fechaExpiracion: { [Op.gte]: fActual } },
    include: [Descuento,
      {
        model: CatalogoServicio,
        include: [TipoEquipo, TipoServicio]
      }],
    order: [['id', 'asc']]
  })
  if (!promocion) {
    throwException('E-promocion-09')
  }
  return promocion
}

async function updatePromocion (id, data, img) {
  const fActual = moment().startOf('day')
  const fInicio = moment(data.fechaInicio, 'DD-MM-YYYY')
  const fExpiracion = moment(data.fechaExpiracion, 'DD-MM-YYYY')
  if (data.fechaInicio) {
    if (fInicio.isBefore(fActual)) {
      throwException('E-promocion-06')
    }
  }

  if (data.fechaInicio || data.fechaExpiracion) {
    if (fInicio.isAfter(fExpiracion)) {
      throwException('E-promocion-07')
    }
  }

  await sequelize.transaction(async t => {
    const promocion = await getPromocion(id)
    console.log(img)
    console.log(data)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'promocion')
    }

    return promocion.update(data, { t })
  })
}

async function difundirPromocion (id, data) {
  if (!data.idCaracteristicasCliente) {
    throwException('E-promocion-08')
  }
  const promocion = await getPromocionVigente(id)
  await sNotificacion.createBulkPromocionNotification('difundir-promocion', data.idCaracteristicasCliente, promocion, data.textoAlternativo)
}

async function deletePromocion (id) {
  await getPromocion(id)
  return Promocion.destroy({
    where: { id }
  })
}

module.exports = {
  createPromocion,
  getPromocion,
  getAllPromocion,
  updatePromocion,
  difundirPromocion,
  deletePromocion,
  getPromocionByServicio,
  getAllPromocionVigente
}

'use strict'
const moment = require('moment')
const Usuario = require('../models').Usuario
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
const Rol = require('../models').Rol
const TipoCaracteristicaCliente = require('../models').TipoCaracteristicaCliente
const CaracteristicaCliente = require('../models').CaracteristicaCliente
const CatalogoServicio = require('../models').CatalogoServicio
const isEmpty = require('../utils/helpers').isEmpty
const Op = require('../models').Sequelize.Op
const mainUrl = require('../config/www.js').url
const port = require('../config/www.js').port
const defaultPlaceholderAvatar = require('../config/path.json').defaultPlaceholderAvatar
const randomatic = require('randomatic')
const secret = require('../config/env').secret
const mailer = require('../utils/mailer')
const jwt = require('jsonwebtoken')
const sEmpresa = require('./empresa')
const sPlantillaNotificacion = require('./plantillaNotificacion')
const sCaracteristicaCliente = require('./caracteristicaCliente')
// const messages = require('../utils/messages')
const fs = require('fs')
const path = require('path')
const pathList = require('../config/path.json')

async function getCliente (id) {
  const usuario = await Usuario.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }]
  })

  if (!usuario) {
    throwException('E-usuario-01')
  }
  return usuario
}

async function getEmpleado (id) {
  const usuario = await Usuario.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [{
      model: Rol,
      where: { tipoRol: { [Op.ne]: 'cliente' } }
    }]
  })

  if (!usuario) {
    throwException('E-usuario-02')
  }
  return usuario
}

async function getAllCliente () {
  return Usuario.findAll({
    order: [['id', 'asc']],
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }]
  })
}

async function getAllEmpleado () {
  return Usuario.findAll({
    order: [['id', 'asc']],
    include: [{
      model: Rol,
      where: { tipoRol: { [Op.ne]: 'cliente' } }
    }]
  })
}

async function getAllClienteWithCaracteristicaCliente (idCaracteristicaCliente) {
  return Usuario.findAll({
    order: [['id', 'asc']],
    include: [{
      model: Rol,
      where: { tipoRol: 'cliente' }
    }, {
      model: CaracteristicaCliente,
      where: { id: { [Op.in]: idCaracteristicaCliente } }
    }]
  })
}

async function createUsuario (data, img, isCliente = undefined) {
  const empresa = await sEmpresa.getEmpresa() // have to load empresa first
  // this is because otherwise it will still create the user, but will fail at email delivery

  if (!data.nombre || !data.apellido || !data.fechaNacimiento || !data.correo) {
    throwException('E-usuario-25')
  }
  const randomContrasena = randomatic('Aa0', 15)
  // randomlhy generated
  data.contrasena = randomContrasena

  let usuario

  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlFoto = handleFile(imgPath, 'usuario')
    } else {
      data.urlFoto = new URL(defaultPlaceholderAvatar, 'http://' + mainUrl + ':' + port).href
    }
    usuario = await Usuario.create(data, { t })
  })

  if (isCliente) {
    console.log(usuario.fechaNacimiento)
    const birthDate = moment(usuario.fechaNacimiento, 'DD-MM-YYYY')
    console.log(birthDate)
    const age = moment().diff(birthDate, 'years')
    console.log(age)
    const autoCaracteristica = await sCaracteristicaCliente.getAutoCaracteristicaClienteByAgeAndSex(age, usuario.sexo)
    usuario.addCaracteristicasCliente(autoCaracteristica)
  }

  let context = {
    fullName: usuario.nombreCompleto,
    password: randomContrasena,
    email: usuario.correo,
    empresa: empresa.nombre,
    rif: empresa.rif,
    direccion: empresa.direccion,
    telefono: empresa.telefono,
    instagram: empresa.instagram,
    facebook: empresa.facebook,
    twitter: empresa.twitter,
    logoEmpresa: empresa.urlLogo,
    date: moment().format('DD-MM-YYYY')
  }
  const recipient = usuario.correo
  return { recipient, context }
}

async function updateUsuario (usuario, data, img) {
  if (data.contrasena) {
    if (!data.contrasenaAnterior) {
      throwException('E-usuario-08') // needs previous password
    }
    if (!await usuario.validPassword(data.contrasenaAnterior)) { // previous password needs to be valid
      throwException('E-usuario-09')
    }
  }
  await sequelize.transaction(async t => {
    if (img) {
      const imgPath = img.path
      data.urlFoto = handleFile(imgPath, 'usuario')
    } if (data.foto) {
      let datos = data.foto.split('base64,')
      console.log(datos[0])
      const ext = datos[0].match('data:image/(.+);')
      console.log(ext[1])
      const fileName = randomatic('A', 5) + '.' + ext[1]
      const pathForEntity = pathList['usuario']
      const rootStaticPath = pathList['staticRoot']
      const rootAbsolutePath = path.resolve('.') // path of the parent
      const uniqueFileName = path.basename(fileName) // gets the uniquely generated name from formidable and keeps the extension
      const relativePath = path.join(rootStaticPath, pathForEntity, uniqueFileName)
      const newAbsolutePath = path.join(rootAbsolutePath, relativePath) // just the full path
      // e.g. public\\img\\avatar\\upload_123123123123.png
      fs.writeFile(newAbsolutePath, datos[1], { encoding: 'base64' }, (err) => console.error(err))
      console.log('relativo: ' + relativePath)
      const baseUrl = 'http://' + mainUrl + ':' + port
      data.urlFoto = new URL(relativePath, baseUrl).href // this URL constructor transforms
    }
    return usuario.update(data, { t })
  })
}

async function updateCliente (id, data, img) {
  if (data.idRol) {
    const rol = await Rol.findOne({
      where: { id: data.idRol }
    })
    if (rol.tipoRol !== 'cliente') {
      throwException('E-usuario-07')
    }
  }
  const usuario = await getCliente(id)
  return updateUsuario(usuario, data, img)
}

async function updateEmpleado (id, data, img) {
  if (data.idRol) {
    const rol = await Rol.findOne({
      where: { id: data.idRol }
    })
    if (!rol) {
      throwException('E-usuario-06') // didn't find the role
    }
    if (rol.tipoRol === 'cliente') {
      throwException('E-usuario-05') // an employee will never have a client role
    }
  }
  const usuario = await getEmpleado(id)
  return updateUsuario(usuario, data, img)
}

async function deleteCliente (id) {
  await getCliente(id)
  // using this in order to throw an exception if
  // it does not find the object that's been requested to be deleted
  return Usuario.destroy({
    where: {
      id: id
    }
  })
}
async function deleteEmpleado (id) {
  await getEmpleado(id)
  // using this in order to throw an exception if
  // it does not find the object that's been requested to be deleted
  return Usuario.destroy({
    where: {
      id: id
    }
  })
}

async function createCliente (data, img) {
  if (!data.idRol) {
    data.idRol = '1'
    // if no role is given, the default is automatically assigned
    // Assuming the first Role is actually client, uneditable, undeletable, generated automatically
  }
  // the bottom query needs to be done because the default behaviour is to ignore "cliente" roles
  // so this is made in order to bypass that limitation
  const rol = await Rol.findOne({
    where: { id: data.idRol }
  })

  if (rol.tipoRol !== 'cliente') {
    throwException('E-usuario-07')
  }

  let { recipient, context } = await createUsuario(data, img, true)

  const welcomeTemplate = await sPlantillaNotificacion.getPlantillaNotificacionByName('registro')
  context.bienvenida = welcomeTemplate.descripcion
  console.log(welcomeTemplate.descripcion)
  await mailer.sendWelcomeEmail(recipient, context)
}

async function createEmpleado (data, img) {
  if (!data.idRol) {
    throwException('E-usuario-06')
  }
  const rol = await Rol.findOne({
    where: { id: data.idRol }
  })
  if (!rol) {
    throwException('E-rol-01')
  }
  if (rol.tipoRol === 'cliente') {
    throwException('E-usuario-03') // they're trying to create a client with the employee route
  }
  let { recipient, context } = await createUsuario(data, img)
  context.role = rol.nombre // add role to context
  await mailer.sendWelcomeEmailEmployee(recipient, context)
}

async function addCaracteristicasCliente (id, data) {
  const cliente = await getCliente(id)
  if (isEmpty(data.caracteristicasCliente)) {
    throwException('E-usuario-26')
  }

  if (data.caracteristicasCliente) {
    console.log(data.caracteristicasCliente)
    return cliente.addCaracteristicasCliente(data.caracteristicasCliente)
  }
}

async function setCaracteristicasCliente (id, data) {
  const cliente = await getCliente(id)
  const carac = await getCaracteristicasCliente(id, 'true')
  const autogenerated = carac.filter(c => c.tipoCaracteristicaCliente.nombre === 'autoperfilado')
  console.log(data.caracteristicasCliente)

  if (data.caracteristicasCliente) {
    const newCarac = data.caracteristicasCliente.concat(autogenerated)
    console.log(newCarac)
    return cliente.setCaracteristicasCliente(newCarac)
  }
}

async function removeCaracteristicasCliente (id, caracteristica) {
  const cliente = await getCliente(id)
  const car = await CaracteristicaCliente.findOne({ where: { id: caracteristica } })
  if (!car) {
    throwException('E03101')
  }
  return cliente.removeCaracteristicasCliente(caracteristica)
}

async function getCaracteristicasCliente (id, query) {
  let conditions = { nombre: { [Op.not]: 'autoperfilado' } }
  if (query.autoperfilado === 'true') {
    conditions = undefined
  }
  await getCliente(id)
  return CaracteristicaCliente.findAll({
    order: [['id', 'asc']],
    include: [{
      model: TipoCaracteristicaCliente,
      where: conditions
    }, {
      model: Usuario,
      where: { id }
    }]
  })
}

async function getUnaddedCaracteristicasCliente (id) {
  let caracteristicas = await CaracteristicaCliente.findAll({
    include: [{
      model: Usuario,
      where: { id: id }
    }, TipoCaracteristicaCliente]
  }).map(car => car.get('id'))
  console.log(caracteristicas)

  if (caracteristicas.length === 0) {
    return CaracteristicaCliente.findAll({
      include: [Usuario]
    })
  }

  return CaracteristicaCliente.findAll({
    where: {
      id: {
        [Op.notIn]: caracteristicas
      }
    },
    include: [Usuario, TipoCaracteristicaCliente]
  })
}

async function addEspecialidad (id, data) {
  const empleado = await getEmpleado(id)
  if (isEmpty(data.catalogoServicio)) {
    throwException('E-usuario-27')
  }
  if (data.catalogoServicio) {
    console.log(data.catalogoServicio)
    return empleado.addCatalogosServicio(data.catalogoServicio)
  }
}

async function setEspecialidad (id, data) {
  const empleado = await getEmpleado(id)
  if (data.catalogoServicio) {
    console.log(data.catalogoServicio)
    return empleado.setCatalogosServicio(data.catalogoServicio)
  }
}

async function removeEspecialidad (id, catalogoServicio) {
  const empleado = await getEmpleado(id)
  return empleado.removeCatalogosServicio(catalogoServicio)
}

async function getEspecialidad (id) {
  const empleado = await getEmpleado(id)
  return empleado.getCatalogosServicio()
}

async function getUnaddedEspecialidad (id) {
  let especialidades = await CatalogoServicio.findAll({
    include: [{
      model: Usuario,
      where: { id: id }
    }]
  }).map(esp => esp.get('id'))
  console.log('alo')
  console.log(especialidades)

  if (especialidades.length === 0) {
    return CatalogoServicio.findAll({
      include: [Usuario]
    })
  }

  return CatalogoServicio.findAll({
    where: {
      id: {
        [Op.notIn]: especialidades
      }
    },
    include: [Usuario]
  })
}

async function verifyUser (data) {
  if (!data.correo || !data.contrasena) {
    throwException('E-usuario-20')
  }

  const usuario = await Usuario.findOne({
    where: { correo: data.correo },
    order: [['id', 'asc']],
    include: [Rol]
  })

  if (!usuario) {
    throwException('E-usuario-21')
  }

  if (await usuario.validPassword(data.contrasena)) {
    const token = await jwt.sign({ correo: usuario.correo }, secret)
    return { usuario, token }
  }
  throwException('E-usuario-21')
}

async function generatePasswordResetToken (correo) {
  if (!correo) {
    throwException('E-usuario-22')
  }

  const usuario = await Usuario.findOne({
    where: { correo: correo }
  })

  if (!usuario) {
    throwException('E-usuario-22')
  }

  const passResetToken = await jwt.sign({ correo: correo }, secret, { expiresIn: '1d' })
  await usuario.update({ resetToken: passResetToken })
  const baseUrl = 'http://' + mainUrl + ':' + port
  const publicUrl = new URL(`/usuario/reiniciarContrasena/${passResetToken}`, baseUrl).href // this URL constructor transforms
  const empresa = await sEmpresa.getEmpresa() // have to load empresa first

  let context = {
    fullName: usuario.nombreCompleto,
    url: publicUrl,
    email: usuario.correo,
    empresa: empresa.nombre,
    rif: empresa.rif,
    direccion: empresa.direccion,
    telefono: empresa.telefono,
    instagram: empresa.instagram,
    facebook: empresa.facebook,
    twitter: empresa.twitter,
    logoEmpresa: empresa.urlLogo,
    date: moment().format('DD-MM-YYYY')
  }
  const recipient = usuario.correo
  await mailer.sendResetPasswordEmail(recipient, context)
}

async function usePasswordResetToken (token) {
  const newPassword = randomatic('Aa0', 15)

  jwt.verify(token, secret, function (err, decoded) {
    if (err) {
      if (err.name === 'TokenExpiredError') { // one day has already happened
        throwException('E-usuario-23')
      }
      console.error(err)
      throwException('E-usuario-24') // some other error
    }
    // decoded here
  })
  const empresa = await sEmpresa.getEmpresa() // have to load empresa first
  const usuario = await Usuario.findOne({
    where: { resetToken: token },
    order: [['id', 'asc']],
    include: [Rol]
  })

  if (!usuario) {
    throwException('E-usuario-24')
  }
  await usuario.update({
    contrasena: newPassword,
    resetToken: null
  })
  const recipient = usuario.correo
  const context = {
    fullName: usuario.nombreCompleto,
    password: newPassword,
    email: usuario.correo,
    empresa: empresa.nombre,
    rif: empresa.rif,
    direccion: empresa.direccion,
    telefono: empresa.telefono,
    instagram: empresa.instagram,
    facebook: empresa.facebook,
    twitter: empresa.twitter,
    logoEmpresa: empresa.urlLogo,
    date: moment().format('DD-MM-YYYY')
  }
  console.log(`${recipient} ${newPassword}`)
  await mailer.sendNewPasswordEmail(recipient, context)
}

async function unsubscribeEmail (id) {
  const usuario = await Usuario.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [Rol]
  })

  if (!usuario) {
    throwException('E-usuario-24')
  }
  await usuario.update({
    suscritoEmail: false
  })
}

module.exports = {
  createEmpleado,
  createCliente,
  getEmpleado,
  getCliente,
  getAllEmpleado,
  getAllCliente,
  getAllClienteWithCaracteristicaCliente,
  updateEmpleado,
  updateCliente,
  deleteEmpleado,
  deleteCliente,
  addCaracteristicasCliente,
  setCaracteristicasCliente,
  removeCaracteristicasCliente,
  getCaracteristicasCliente,
  getUnaddedCaracteristicasCliente,
  addEspecialidad,
  setEspecialidad,
  removeEspecialidad,
  getEspecialidad,
  getUnaddedEspecialidad,
  verifyUser,
  generatePasswordResetToken,
  usePasswordResetToken,
  unsubscribeEmail
}

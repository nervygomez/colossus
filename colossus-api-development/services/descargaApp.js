'use strict'

const DescargaApp = require('../models').DescargaApp
const throwException = require('../utils/helpers').throwException
const handleFile = require('../utils/fileHandler').handleFile
const sequelize = require('../models').sequelize
async function createDescargaApp (data, img) {
  if (!img) {
    throwException('E-descargaApp-01')
  }
  const imgPath = img.path

  await sequelize.transaction(async t => {
    data.urlImagen = handleFile(imgPath, 'descargaApp')
    await DescargaApp.create(data, { transaction: t })
  })
  // IMAGEN AQUI
}

async function getDescargaApp (id) {
  const descargaApp = await DescargaApp.findOne({
    where: { id }
  })
  if (!descargaApp) {
    throwException('E-descargaApp-02')
  }
  return descargaApp
}
async function updateDescargaApp (id, data, img) {
  await sequelize.transaction(async t => {
    const descargaApp = await getDescargaApp(id)

    if (img) {
      const imgPath = img.path
      data.urlImagen = handleFile(imgPath, 'descargaApp')
    }

    return descargaApp.update(data, { transaction: t })
  })
}

async function getAllDescargaApp () {
  const descargaApp = await DescargaApp.findAll({
    order: [['id', 'asc']]
  })
  if (!descargaApp) {
    throwException('E-descargaApp-03')
  }
  return descargaApp
}
module.exports = {
  createDescargaApp,
  updateDescargaApp,
  getAllDescargaApp,
  getDescargaApp

}

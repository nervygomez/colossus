'use strict'
// entidad 25
const CaracteristicaCliente = require('../models').CaracteristicaCliente
const throwException = require('../utils/helpers').throwException
const sTipoCaracteristicaCliente = require('./tipoCaracteristicaCliente')
const TipoCaracteristicaCliente = require('../models').TipoCaracteristicaCliente
const Op = require('../models').Sequelize.Op

// const messages = require('../utils/messages')

async function addCaracteristicaCliente (data) {
  if (!data.idTipoCaracteristicaCliente) {
    throwException('E02502') // needs the association
  }
  await sTipoCaracteristicaCliente.getTipoCaracteristicaCliente(data.idTipoCaracteristicaCliente)

  console.log(data)
  return CaracteristicaCliente.create(data)
}

async function getAllCaracteristicaCliente (query) {
  let conditions = { nombre: { [Op.not]: 'autoperfilado' } }
  if (query.autoperfilado === 'true') {
    conditions = undefined
  }
  const findall = await CaracteristicaCliente.findAll({
    order: [['id', 'asc']],
    include: [{
      model: TipoCaracteristicaCliente,
      where: conditions
    }]
  })
  return findall
}

async function getCaracteristicaCliente (id) {
  const caracteristicaCliente = await CaracteristicaCliente.findOne({
    where: { id },
    order: [['id', 'asc']],
    include: [TipoCaracteristicaCliente]

  })

  if (!caracteristicaCliente) {
    throwException('E02501')
  }
  return caracteristicaCliente
}

async function updateCaracteristicaCliente (id, data) {
  const caracteristicaCliente = await getCaracteristicaCliente(id)
  if (!caracteristicaCliente) {
    throwException('E02501')
  }
  if (data.idTipoCaracteristicaCliente) {
    await sTipoCaracteristicaCliente.getTipoCaracteristicaCliente(data.idTipoCaracteristicaCliente)
  }

  return caracteristicaCliente.update(data)
}

async function deleteCaracteristicaCliente (id) {
  await getCaracteristicaCliente(id)
  return CaracteristicaCliente.destroy({
    where: {
      id: id
    }
  })
}

async function getUsuariosCaracteristicaCliente (id) {
  const caracteristicaCliente = await getCaracteristicaCliente(id)
  return caracteristicaCliente.getUsuarios()
}

async function getAutoCaracteristicaClienteByAgeAndSex (age, sex) {
  let nombre
  if (age < 15) { // less than 15
    nombre = (sex === 'M') ? 'Mujer menor de 15' : 'Hombre menor de 15'
  } else if (age < 20) { // between 15 and 20
    nombre = (sex === 'M') ? 'Mujer entre 15 y 20' : 'Hombre entre 15 y 20'
  } else if (age < 30) { // between 20 and 30
    nombre = (sex === 'M') ? 'Mujer entre 20 y 30' : 'Hombre entre 20 y 30'
  } else if (age < 40) { // between 30 and 40
    nombre = (sex === 'M') ? 'Mujer entre 30 y 40' : 'Hombre entre 30 y 40'
  } else if (age < 50) { // between 30 and 50
    nombre = (sex === 'M') ? 'Mujer entre 40 y 50' : 'Hombre entre 40 y 50'
  } else if (age < 60) { // between 50 and 60
    nombre = (sex === 'M') ? 'Mujer entre 50 y 60' : 'Hombre entre 50 y 60'
  } else { // more than 60
    nombre = (sex === 'M') ? 'Mujer mayor de 60' : 'Hombre mayor de 60'
  }
  return CaracteristicaCliente.findOne({
    where: { nombre }
  })
}

module.exports = {
  addCaracteristicaCliente,
  getCaracteristicaCliente,
  getAllCaracteristicaCliente,
  updateCaracteristicaCliente,
  deleteCaracteristicaCliente,
  getUsuariosCaracteristicaCliente,
  getAutoCaracteristicaClienteByAgeAndSex
}

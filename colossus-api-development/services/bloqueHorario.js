'use strict'

const BloqueHorario = require('../models').BloqueHorario
const throwException = require('../utils/helpers').throwException
const Op = require('../models').Sequelize.Op

async function createBloqueHorario (data) {
  console.log(data)
  if (data.horaInicio > data.horaFinal) {
    throwException('E04202')
  }
  await overlapCheck(data.horaInicio, data.horaFinal)
  return BloqueHorario.create(data)
}

async function overlapCheck (inicio, final) {
  const bloqueHorario = await BloqueHorario.count({
    where: {
      [Op.or]: [
        // horaInicio (others), horaFinal (others), inicio (ours), final (ours)
        { horaInicio: { [Op.gt]: inicio }, horaFinal: { [Op.lt]: final } },
        // horaInicio >= inicio and horaFinal <= final
        // starts later, finishes earlier. This one might be a special case of the second and third validations
        // this mean the bloqueHorario (BH) we're trying to create encapsulates another one
        { horaInicio: { [Op.gt]: inicio, [Op.lt]: final } },
        // horaInicio >= inicio and horaInicio <= final (inicio>=horaInicio>=final)
        // starts between the start and ending hours of another BH
        // the means it begins within another BH
        { horaFinal: { [Op.gt]: inicio, [Op.lt]: final } },
        // hora final >= inicio and horaFinal <= final
        // finishes between the start and ending hours of another BH
        // this means our BH starts in the middle of another BH and ends before it ends
        { horaInicio: { [Op.lt]: inicio }, horaFinal: { [Op.gt]: final } }
        // horaInicio <= inicio and horaFinal>= final
        // encapsulates another BH completely
        // this means our bh starts after another one has started, but before it has finished
      ]
    }
  })
  if (bloqueHorario !== 0) {
    throwException('E04203')
  }
}

async function getBloqueHorario (id) {
  const bloqueHorario = await BloqueHorario.findOne({
    where: { id }
  })
  if (!bloqueHorario) {
    throwException('E04201')
  }
  return bloqueHorario
}

async function getAllBloqueHorario () {
  return BloqueHorario.findAll({
    order: [['id', 'asc']]
  })
}

async function updateBloqueHorario (id, data) {
  const bloqueHorario = await getBloqueHorario(id)
  if (!bloqueHorario) {
    throwException('E04201')
  }
  if (data.horaInicio > data.horaFinal) {
    throwException('E04202')
  }
  return bloqueHorario.update(data)
}

async function deleteBloqueHorario (id) {
  await getBloqueHorario(id)
  return BloqueHorario.destroy({
    where: { id }
  })
}

module.exports = {
  createBloqueHorario,
  getBloqueHorario,
  getAllBloqueHorario,
  updateBloqueHorario,
  deleteBloqueHorario }

'use strict'
const Garantia = require('../models').Garantia
const OrdenServicio = require('../models').OrdenServicio
const throwException = require('../utils/helpers').throwException
const Condicion = require('../models').Condicion
const sDuracionGarantia = require('../services/duracionGarantia')
const DuracionGarantia = require('../models').DuracionGarantia
const moment = require('moment')

async function createGarantia (idOrdenServicio, data, transaction = undefined) {
  const duracionGarantia = await sDuracionGarantia.getDuracionGarantia(data.idDuracionGarantia)
  const garantiaObj = {
    idOrdenServicio,
    idDuracionGarantia: data.idDuracionGarantia,
    fechaExpiracion: moment().add(duracionGarantia.dia, 'days').format('DD/MM/YYYY')
  }
  let garantia = await Garantia.create(garantiaObj, { transaction })
  await garantia.addCondiciones(data.condiciones, { transaction })
  // NOTIFICATION BELOW
}

async function getGarantia (id) {
  const garantia = await Garantia.findOne({
    where: { id },
    include: [ OrdenServicio, Condicion, DuracionGarantia ]
  })
  if (!garantia) {
    throwException('E-garantia-01')
  }
  return garantia
}

async function getAllGarantia () {
  const garantia = Garantia.findAll({
    include: [OrdenServicio, DuracionGarantia],
    required: true,
    order: [['id', 'asc']]
  })
  console.log(garantia)
  if (!garantia) {
    throwException('E-garantia-02')
  }
  return garantia
}

async function getGarantiaByOrdenServicio (idOrdenServicio) {
  const garantia = await Garantia.findOne({
    where: { idOrdenServicio },
    include: [ OrdenServicio, Condicion, DuracionGarantia ]
  })
  if (!garantia) {
    throwException('E-garantia-03')
  }
  return garantia
}

module.exports = { createGarantia,
  getGarantia,
  getAllGarantia,
  getGarantiaByOrdenServicio
}

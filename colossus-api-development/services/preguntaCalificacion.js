'use strict'

const PreguntaCalificacion = require('../models').PreguntaCalificacion
const throwException = require('../utils/helpers').throwException

async function createPreguntaCalificacion (data) {
  console.log(data)
  await PreguntaCalificacion.create(data)
}

async function getPreguntaCalificacion (id) {
  const preguntaCalificacion = await PreguntaCalificacion.findOne({
    where: { id }
  })
  if (!preguntaCalificacion) {
    throwException('E-preguntaCalificacion-01')
  }
  return preguntaCalificacion
}

async function getAllPreguntaCalificacion () {
  return PreguntaCalificacion.findAll({
    order: [['id', 'asc']]
  })
}

async function updatePreguntaCalificacion (id, data) {
  const preguntaCalificacion = await getPreguntaCalificacion(id)
  return preguntaCalificacion.update(data)
}

async function deletePreguntaCalificacion (id) {
  await getPreguntaCalificacion(id)
  return PreguntaCalificacion.destroy({
    where: { id }
  })
}

module.exports = { createPreguntaCalificacion,
  getPreguntaCalificacion,
  getAllPreguntaCalificacion,
  updatePreguntaCalificacion,
  deletePreguntaCalificacion }

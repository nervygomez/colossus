var router = require('express').Router()
var cTipoServicio = require('../controllers/tipoServicio')

router.post('/', cTipoServicio.createTipoServicio)
router.get('/:id', cTipoServicio.getTipoServicio)
router.get('/', cTipoServicio.getAllTipoServicio)
router.put('/:id', cTipoServicio.updateTipoServicio)
router.delete('/:id', cTipoServicio.deleteTipoServicio)

module.exports = router

var router = require('express').Router()
var cReporte = require('../controllers/reporte')

router.get('/solicitudServicio', cReporte.reporteSolicitud)
router.get('/garantia', cReporte.reporteGarantia)
router.get('/revision', cReporte.reporteRevision)
router.get('/cliente', cReporte.reporteCliente)
router.get('/estadisticoSolicitud', cReporte.reporteEstadisticoSolicitud)
router.get('/incidencia', cReporte.reporteIncidencia)
router.get('/estadisticoTecnico', cReporte.reporteEstadisticoTecnico)
router.get('/estadisticoTecnico2', cReporte.reporteEstadisticoTecnico2)
router.get('/estadisticoAnual', cReporte.reporteEstadisticoAnual)
router.get('/estadisticoTiempoProm', cReporte.reporteEstadisticoTiempoProm)

module.exports = router

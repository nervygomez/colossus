'use strict'

module.exports = function (app) {
  app.use('/marca', require('./marca'))
  app.use('/tipoEquipo', require('./tipoEquipo'))
  app.use('/categoriaTipoEquipo', require('./categoriaTipoEquipo'))
  app.use('/tipoServicio', require('./tipoServicio'))
  app.use('/rol', require('./rol'))
  app.use('/cliente', require('./cliente'))
  app.use('/empleado', require('./empleado'))
  app.use('/empresa', require('./empresa'))
  app.use('/modeloEquipo', require('./modeloEquipo'))
  app.use('/catalogoServicio', require('./catalogoServicio'))
  app.use('/tipoIncidencia', require('./tipoIncidencia'))
  app.use('/permiso', require('./permiso'))
  app.use('/tipoReclamo', require('./tipoReclamo'))
  app.use('/tipoCaracteristicaCliente', require('./tipoCaracteristicaCliente'))
  app.use('/caracteristicaCliente', require('./caracteristicaCliente'))
  app.use('/actividad', require('./actividad'))
  app.use('/bloqueHorario', require('./bloqueHorario'))
  app.use('/agenda', require('./agenda'))
  app.use('/solicitudServicio', require('./solicitudServicio'))
  app.use('/ordenServicio', require('./ordenServicio'))
  app.use('/revision', require('./revision'))
  app.use('/login', require('./login'))
  app.use('/usuario', require('./usuario'))
  app.use('/sliderMovil', require('./sliderMovil'))
  app.use('/caracteristicaEmpresa', require('./caracteristicaEmpresa'))
  app.use('/precio', require('./precio'))
  app.use('/bitacora', require('./bitacora'))
  app.use('/descuento', require('./descuento'))
  app.use('/promocion', require('./promocion'))
  app.use('/condicion', require('./condicion'))
  app.use('/garantia', require('./garantia'))
  app.use('/tipoComentario', require('./tipoComentario'))
  app.use('/comentario', require('./comentario'))
  app.use('/motivoRechazo', require('./motivoRechazo'))
  app.use('/tecnicoExhibicion', require('./tecnicoExhibicion'))
  app.use('/duracionGarantia', require('./duracionGarantia'))
  app.use('/preguntaCalificacion', require('./preguntaCalificacion'))
  app.use('/preguntaFrecuente', require('./preguntaFrecuente'))
  app.use('/servicioExhibicion', require('./servicioExhibicion'))
  app.use('/plantillaNotificacion', require('./plantillaNotificacion'))
  app.use('/descargaApp', require('./descargaApp'))
  app.use('/carrusel', require('./carrusel'))
  app.use('/horarioTrabajo', require('./horarioTrabajo'))
  app.use('/reporte', require('./reporte'))
  app.use('/notificacion', require('./notificacion'))
  app.use('/reclamoServicio', require('./reclamoServicio'))
  app.use('/mensaje', require('./mensaje'))
}

var router = require('express').Router()
var cOrdenServicio = require('../controllers/ordenServicio')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.get('/calificacionServicio', cOrdenServicio.getAllCalificacionServicio)
router.get('/enReparacion', ejwt, checkUser, cOrdenServicio.getAllOrdenServicioRepairing)
router.get('/sinCalificarServicioCliente', ejwt, checkUser, cOrdenServicio.getSinCalificarServicioCliente)
router.get('/', cOrdenServicio.getAllOrdenServicio)
router.get('/:id', cOrdenServicio.getOrdenServicio)

router.get('/:id/actividadesPorAgendar', cOrdenServicio.getSchedulableTasks)
router.get('/:id/agendasPorCompletar', ejwt, checkUser, cOrdenServicio.getUncompletedAgendas)
router.post('/:id/completarReparacion', cOrdenServicio.completeRepair)
router.get('/:id/garantia', cOrdenServicio.getGarantiaByOrdenServicio)
router.post('/:id/crearGarantia', cOrdenServicio.setAsDelivered)
router.post('/:id/calificacionServicio', cOrdenServicio.createCalificacionServicio)
router.get('/:id/calificacionServicio', cOrdenServicio.getCalificacionServicio)
router.post('/:id/entregarSinGarantia', cOrdenServicio.updateOrdenEntregarSinGarantia)
module.exports = router

var router = require('express').Router()
var cTipoEquipo = require('../controllers/tipoEquipo')

router.post('/', cTipoEquipo.createTipoEquipo)
router.get('/:id', cTipoEquipo.getTipoEquipo)
router.get('/', cTipoEquipo.getAllTipoEquipo)
router.put('/:id', cTipoEquipo.updateTipoEquipo)
router.delete('/:id', cTipoEquipo.deleteTipoEquipo)
router.get('/:id/marcas', cTipoEquipo.getMarcasTipoEquipo)
router.get('/:id/catalogosServicio', cTipoEquipo.getCatalogosTipoEquipo)

module.exports = router

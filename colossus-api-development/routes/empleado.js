var router = require('express').Router()
var cUsuario = require('../controllers/usuario')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.post('/', cUsuario.createEmpleado)
router.get('/incidencias', ejwt, checkUser, cUsuario.getIncidenciasByTecnico)
router.get('/:id', cUsuario.getEmpleado)
router.get('/', cUsuario.getAllEmpleado)
router.put('/:id', cUsuario.updateEmpleado)
router.delete('/:id', cUsuario.deleteEmpleado)
router.get('/:id/especialidad', cUsuario.getEspecialidad)
router.get('/:id/especialidadNoAgregada', cUsuario.getUnaddedEspecialidad)
router.post('/:id/especialidad', cUsuario.addEspecialidad)
router.post('/:id/setEspecialidad', cUsuario.setEspecialidad)
router.delete('/:id/especialidad/:catalogoServicio', cUsuario.removeEspecialidad)
module.exports = router

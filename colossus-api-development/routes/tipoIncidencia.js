var router = require('express').Router()
var cTipoIncidencia = require('../controllers/tipoIncidencia')

router.post('/', cTipoIncidencia.createTipoIncidencia)
router.get('/:id', cTipoIncidencia.getTipoIncidencia)
router.get('/', cTipoIncidencia.getAllTipoIncidencia)
router.put('/:id', cTipoIncidencia.updateTipoIncidencia)
router.delete('/:id', cTipoIncidencia.deleteTipoIncidencia)

module.exports = router

var router = require('express').Router()
var cPrecio = require('../controllers/precio')

router.get('/', cPrecio.getPrecio)
router.put('/', cPrecio.updatePrecio)

module.exports = router

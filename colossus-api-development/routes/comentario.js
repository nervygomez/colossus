var router = require('express').Router()
var cComentario = require('../controllers/comentario')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.post('/usuarioNoRegistrado', cComentario.createComentarioNoRegistado)
router.post('/', ejwt, checkUser, cComentario.createComentario)
router.put('/:id', ejwt, checkUser, cComentario.updateComentarioRespuesta)
router.get('/', cComentario.getAllComentario)
router.get('/sinResponder', ejwt, checkUser, cComentario.getComentarioSinResponder)
router.get('/usuario', ejwt, checkUser, cComentario.getComentarioByUser)
router.delete('/:id', ejwt, checkUser, cComentario.deleteComentario)

module.exports = router

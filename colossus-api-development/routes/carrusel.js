var express = require('express')

var cCarrusel = require('../controllers/carrusel')

var router = express.Router()

router.get('/:id', cCarrusel.getCarrusel)
router.post('/', cCarrusel.createCarrusel)
router.get('/', cCarrusel.getAllCarrusel)
router.put('/:id', cCarrusel.updateCarrusel)
router.delete('/:id', cCarrusel.deleteCarrusel)

module.exports = router

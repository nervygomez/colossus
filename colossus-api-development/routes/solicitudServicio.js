var router = require('express').Router()
var cSolicitudServicio = require('../controllers/solicitudServicio')
const cBitacora = require('../controllers/bitacora')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.post('/', cSolicitudServicio.createSolicitudServicio)
router.get('/bitacora', cBitacora.getAllBitacora)
router.get('/conGarantia', ejwt, checkUser, cSolicitudServicio.getSolicitudGarantiaVigente)
router.get('/sinGarantia', ejwt, checkUser, cSolicitudServicio.getSolicitudSinGarantia)
router.get('/cliente', ejwt, checkUser, cSolicitudServicio.getAllSolicitudServicioCliente)
router.get('/presupuestoCliente', ejwt, checkUser, cSolicitudServicio.getAllPresupuestoCliente)
router.get('/', ejwt, checkUser, cSolicitudServicio.getAllSolicitudServicio)
router.get('/debug', cSolicitudServicio.getAllDebug)
router.get('/:id', ejwt, cSolicitudServicio.getSolicitudServicio)
router.post('/:id/rechazarSolicitud', cSolicitudServicio.denySolicitudServicio)
router.post('/:id/aceptarSolicitud', cSolicitudServicio.acceptSolicitudServicio)
router.post('/:id/revisar', ejwt, checkUser, cSolicitudServicio.finishRevision)
router.post('/:id/generarPresupuesto', ejwt, checkUser, cSolicitudServicio.generateBudget)
router.get('/:id/empleadosDisponibles', cSolicitudServicio.getAvailableEmpleadosForAcceptance)
router.post('/:id/aceptarPresupuesto', ejwt, checkUser, cSolicitudServicio.acceptBudget)
router.post('/:id/rechazarPresupuesto', ejwt, checkUser, cSolicitudServicio.rejectBudget)
router.get('/:id/bitacora', cBitacora.getBitacora)
module.exports = router

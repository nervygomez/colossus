var router = require('express').Router()
var cEmpresa = require('../controllers/empresa')

router.post('/', cEmpresa.createEmpresa)
router.get('/', cEmpresa.getEmpresa)
router.put('/', cEmpresa.updateEmpresa)

module.exports = router

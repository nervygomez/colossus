var router = require('express').Router()
var cPreguntaCalificacion = require('../controllers/preguntaCalificacion')

router.post('/', cPreguntaCalificacion.createPreguntaCalificacion)
router.get('/', cPreguntaCalificacion.getAllPreguntaCalificacion)
router.get('/:id', cPreguntaCalificacion.getPreguntaCalificacion)
router.put('/:id', cPreguntaCalificacion.updatePreguntaCalificacion)
router.delete('/:id', cPreguntaCalificacion.deletePreguntaCalificacion)

module.exports = router

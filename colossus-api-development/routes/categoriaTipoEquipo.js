var express = require('express')

var cCategoriaTipoEquipo = require('../controllers/categoriaTipoEquipo')

var router = express.Router()

router.get('/:id', cCategoriaTipoEquipo.getCategoriaTipoEquipo)
router.post('/', cCategoriaTipoEquipo.createCategoriaTipoEquipo)
router.get('/', cCategoriaTipoEquipo.getAllCategoriaTipoEquipo)
router.put('/:id', cCategoriaTipoEquipo.updateCategoriaTipoEquipo)
router.delete('/:id', cCategoriaTipoEquipo.deleteCategoriaTipoEquipo)

module.exports = router

var router = require('express').Router()

var cActividad = require('../controllers/actividad')

router.post('/', cActividad.createActividad)
router.get('/:id', cActividad.getActividad)
router.get('/', cActividad.getAllActividad)
router.put('/:id', cActividad.updateActividad)
router.delete('/:id', cActividad.deleteActividad)

module.exports = router

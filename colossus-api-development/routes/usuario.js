var router = require('express').Router()
var cUsuario = require('../controllers/usuario')

router.post('/reiniciarContrasena', cUsuario.forgotPassword)
router.get('/reiniciarContrasena/:token', cUsuario.resetPassword)
router.get('/:id/unsubscribe/', cUsuario.unsubscribeEmail)

module.exports = router

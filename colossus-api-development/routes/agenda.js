var router = require('express').Router()
var cAgenda = require('../controllers/agenda')
const ejwt = require('../middlewares/auth').expressjwt
const checkUser = require('../middlewares/auth').checkUser

router.post('/', cAgenda.createAgenda)
router.get('/tecnicosDisponibles', cAgenda.getAvailableEmpleados)
router.get('/misAgendas', ejwt, checkUser, cAgenda.getAgendasForUser)
router.get('/misAgendasPendientes', ejwt, checkUser, cAgenda.getPendingAgendasForUser)

router.get('/', cAgenda.getAllAgenda)
router.get('/:id', cAgenda.getAgenda)
router.put('/:id', cAgenda.updateAgenda)
router.delete('/:id', cAgenda.deleteAgenda)
router.post('/:id/completar', cAgenda.completeAgenda)
router.post('/:id/reagendar', cAgenda.rescheduleAgenda)

module.exports = router

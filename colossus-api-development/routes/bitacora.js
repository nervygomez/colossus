const router = require('express').Router()
const cBitacora = require('../controllers/bitacora')

router.post('/', cBitacora.createBitacora)
router.get('/:id', cBitacora.getBitacora)
router.get('/', cBitacora.getAllBitacora)

module.exports = router

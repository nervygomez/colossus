var express = require('express')

var cMensaje = require('../controllers/mensaje')

var router = express.Router()

router.get('/:id', cMensaje.getMensaje)
router.get('/', cMensaje.getAllMensaje)
router.put('/:id', cMensaje.updateMensaje)

module.exports = router

const sTipoServicio = require('../services/tipoServicio')
const responses = require('../utils/responses')

async function createTipoServicio (req, res) {
  try {
    await sTipoServicio.createTipoServicio(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I00401')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllTipoServicio (req, res) {
  try {
    const tipoServicio = await sTipoServicio.getAllTipoServicio()
    responses.makeResponseOk(res, { tipoServicio }, 'tipoServicio/getAllTipoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getTipoServicio (req, res) {
  try {
    const id = req.params.id
    const tipoServicio = await sTipoServicio.getTipoServicio(id)
    responses.makeResponseOk(res, { tipoServicio }, 'tipoServicio/getTipoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoServicio (req, res) {
  try {
    const tipoServicio = await sTipoServicio.updateTipoServicio(req.params.id, req.fields)
    responses.makeResponseOk(res, { tipoServicio }, 'tipoServicio/getTipoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoServicio (req, res) {
  try {
    await sTipoServicio.deleteTipoServicio(req.params.id)
    responses.makeResponseOkMessage(res, 'I00403')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createTipoServicio,
  getAllTipoServicio,
  getTipoServicio,
  updateTipoServicio,
  deleteTipoServicio }

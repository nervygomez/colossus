'use strict'
const sReclamoServicio = require('../services/reclamoServicio')
const responses = require('../utils/responses')

async function createReclamoServicio (req, res) {
  try {
    await sReclamoServicio.createReclamoServicio(req.fields)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllReclamoServicio (req, res) {
  try {
    const reclamoServicio = await sReclamoServicio.getAllReclamoServicio(req.currentUser, req.query)
    responses.makeResponseOk(res, { reclamoServicio }, 'reclamoServicio/getAllReclamoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getReclamoServicio (req, res) {
  try {
    const reclamoServicio = await sReclamoServicio.getReclamoServicio(req.params.id)
    responses.makeResponseOk(res, { reclamoServicio }, 'reclamoServicio/getReclamoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function acceptReclamoServicio (req, res) {
  try {
    await sReclamoServicio.acceptReclamoServicio(req.params.id)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function acceptRevisionByReclamoServicio (req, res) {
  try {
    await sReclamoServicio.acceptRevisionByReclamoServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-04')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function rejectRevisionByReclamoServicio (req, res) {
  try {
    await sReclamoServicio.rejectRevisionByReclamoServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-05')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function rejectReclamoServicio (req, res) {
  try {
    await sReclamoServicio.rejectReclamoServicio(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function completeReclamoRepair (req, res) {
  try {
    await sReclamoServicio.completeRepair(req.params.id)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-06')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deliverEquipo (req, res) {
  try {
    await sReclamoServicio.entregarReclamo(req.params.id)
    responses.makeResponseOkMessage(res, 'I-reclamoServicio-07')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSchedulableReclamoTasks (req, res) {
  try {
    const { id } = req.params
    const actividad = await sReclamoServicio.getSchedulableReclamoTasks(id)
    responses.makeResponseOk(res, { actividad }, 'actividad/getAllActividad')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getUncompletedAgendas (req, res) {
  try {
    const { id } = req.params
    const agenda = await sReclamoServicio.getUncompletedAgendas(req.currentUser, id)
    responses.makeResponseOk(res, { agenda }, 'agenda/getAllAgenda')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createReclamoServicio,
  getAllReclamoServicio,
  getReclamoServicio,
  acceptReclamoServicio,
  rejectReclamoServicio,
  acceptRevisionByReclamoServicio,
  rejectRevisionByReclamoServicio,
  getSchedulableReclamoTasks,
  getUncompletedAgendas,
  deliverEquipo,
  completeReclamoRepair
}

const sCaracteristicaCliente = require('../services/caracteristicaCliente')
const responses = require('../utils/responses')

async function createCaracteristicaCliente (req, res) {
  try {
    await sCaracteristicaCliente.addCaracteristicaCliente(req.fields)
    responses.makeResponseOkMessage(res, 'I02501')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllCaracteristicaCliente (req, res) {
  try {
    const caracteristicaCliente = await sCaracteristicaCliente.getAllCaracteristicaCliente(req.query)
    responses.makeResponseOk(res, { caracteristicaCliente }, 'caracteristicaCliente/getAllCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getCaracteristicaCliente (req, res) {
  try {
    const { id } = req.params
    const caracteristicaCliente = await sCaracteristicaCliente.getCaracteristicaCliente(id)
    responses.makeResponseOk(res, { caracteristicaCliente }, 'caracteristicaCliente/getCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCaracteristicaCliente (req, res) {
  try {
    const { id } = req.params
    await sCaracteristicaCliente.updateCaracteristicaCliente(id, req.fields)
    responses.makeResponseOkMessage(res, 'I02502')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteCaracteristicaCliente (req, res) {
  try {
    const { id } = req.params
    await sCaracteristicaCliente.deleteCaracteristicaCliente(id)
    responses.makeResponseOkMessage(res, 'I02503')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getUsuariosCaracteristicaCliente (req, res) {
  try {
    const { id } = req.params
    const usuario = await sCaracteristicaCliente.getUsuariosCaracteristicaCliente(id)
    responses.makeResponseOk(res, { usuario }, 'usuario/getAllCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createCaracteristicaCliente,
  getAllCaracteristicaCliente,
  getCaracteristicaCliente,
  updateCaracteristicaCliente,
  deleteCaracteristicaCliente,
  getUsuariosCaracteristicaCliente
}

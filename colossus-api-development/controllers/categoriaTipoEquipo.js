'use strict'

const sCategoriaTipoEquipo = require('../services/categoriaTipoEquipo')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getCategoriaTipoEquipo (req, res) {
  try {
    const id = req.params.id
    const categoriaTipoEquipo = await sCategoriaTipoEquipo.getCategoriaTipoEquipo(id)
    responses.makeResponseOk(res, { categoriaTipoEquipo }, 'categoriaTipoEquipo/getCategoriaTipoEquipo')
  } catch (err) {
    responses.makeResponseException(res, err)
  }
}

async function createCategoriaTipoEquipo (req, res) {
  try {
    // const img = isEmpty(req.files) ? null : req.files.imagen
    // set the image
    await sCategoriaTipoEquipo.createCategoriaTipoEquipo(req.fields)
    responses.makeResponseOkMessage(res, 'I00201')
  } catch (err) {
    console.log(err)
    responses.makeResponseException(res, err)
  }
}

async function getAllCategoriaTipoEquipo (req, res) {
  try {
    const categoriaTipoEquipo = await sCategoriaTipoEquipo.getAllCategoriaTipoEquipo()
    console.log(categoriaTipoEquipo)
    responses.makeResponseOk(res, { categoriaTipoEquipo }, 'categoriaTipoEquipo/getAllCategoriaTipoEquipo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateCategoriaTipoEquipo (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    console.log(img)
    await sCategoriaTipoEquipo.updateCategoriaTipoEquipo(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I00202')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function deleteCategoriaTipoEquipo (req, res) {
  try {
    await sCategoriaTipoEquipo.deleteCategoriaTipoEquipo(req.params.id)
    responses.makeResponseOkMessage(res, 'I00203')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  getCategoriaTipoEquipo,
  createCategoriaTipoEquipo,
  getAllCategoriaTipoEquipo,
  updateCategoriaTipoEquipo,
  deleteCategoriaTipoEquipo
}

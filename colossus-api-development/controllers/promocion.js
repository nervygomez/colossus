const sPromocion = require('../services/promocion')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function createPromocion (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sPromocion.createPromocion(req.fields, img)
    responses.makeResponseOkMessage(res, 'I-promocion-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPromocion (req, res) {
  try {
    const promocion = await sPromocion.getPromocion(req.params.id)
    responses.makeResponseOk(res, { promocion }, 'promocion/getPromocion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getPromocionByServicio (req, res) {
  try {
    const promocion = await sPromocion.getPromocionByServicio(req.params.id)
    responses.makeResponseOk(res, { promocion }, 'promocion/getAllPromocion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllPromocion (req, res) {
  try {
    const promocion = await sPromocion.getAllPromocion(req.query)
    responses.makeResponseOk(res, { promocion }, 'promocion/getAllPromocion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllPromocionVigente (req, res) {
  try {
    const promocion = await sPromocion.getAllPromocionVigente()
    responses.makeResponseOk(res, { promocion }, 'promocion/getAllPromocion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updatePromocion (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sPromocion.updatePromocion(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I-promocion-02')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function difundirPromocion (req, res) {
  try {
    await sPromocion.difundirPromocion(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-promocion-04')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deletePromocion (req, res) {
  try {
    await sPromocion.deletePromocion(req.params.id)
    responses.makeResponseOkMessage(res, 'I-promocion-03')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createPromocion,
  getAllPromocion,
  getPromocion,
  updatePromocion,
  difundirPromocion,
  deletePromocion,
  getPromocionByServicio,
  getAllPromocionVigente
}

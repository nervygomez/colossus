const sTipoIncidencia = require('../services/tipoIncidencia')
const responses = require('../utils/responses')

async function createTipoIncidencia (req, res) {
  try {
    await sTipoIncidencia.createTipoIncidencia(req.fields)
    responses.makeResponseOkMessage(res, 'I01401')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getAllTipoIncidencia (req, res) {
  try {
    const tipoIncidencia = await sTipoIncidencia.getAllTipoIncidencia()
    responses.makeResponseOk(res, { tipoIncidencia }, 'tipoIncidencia/getAllTipoIncidencia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getTipoIncidencia (req, res) {
  try {
    const id = req.params.id
    const tipoIncidencia = await sTipoIncidencia.getTipoIncidencia(id)
    responses.makeResponseOk(res, { tipoIncidencia }, 'tipoIncidencia/getTipoIncidencia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoIncidencia (req, res) {
  try {
    const tipoIncidencia = await sTipoIncidencia.updateTipoIncidencia(req.params.id, req.fields)
    responses.makeResponseOk(res, { tipoIncidencia }, 'tipoIncidencia/getTipoIncidencia')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoIncidencia (req, res) {
  try {
    await sTipoIncidencia.deleteTipoIncidencia(req.params.id)
    responses.makeResponseOkMessage(res, 'I01403')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createTipoIncidencia,
  getTipoIncidencia,
  getAllTipoIncidencia,
  updateTipoIncidencia,
  deleteTipoIncidencia }

const sDescargaApp = require('../services/descargaApp')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function createDescargaApp (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sDescargaApp.createDescargaApp(req.fields, img)
    responses.makeResponseOkMessage(res, 'I-descargaApp-01')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}

async function updateDescargaApp (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sDescargaApp.updateDescargaApp(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I-descargaApp-02')
  } catch (e) {
    console.log(e)
    responses.makeResponseException(res, e)
  }
}
async function getDescargaApp (req, res) {
  try {
    const id = req.params.id
    const descargaApp = await sDescargaApp.getDescargaApp(id)
    responses.makeResponseOk(res, { descargaApp }, 'descargaApp/getDescargaApp')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllDescargaApp (req, res) {
  try {
    const descargaApp = await sDescargaApp.getAllDescargaApp()
    responses.makeResponseOk(res, { descargaApp }, 'descargaApp/getAllDescargaApp')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createDescargaApp,
  updateDescargaApp,
  getDescargaApp,
  getAllDescargaApp

}

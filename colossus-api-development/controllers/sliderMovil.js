const sSliderMovil = require('../services/sliderMovil')
const responses = require('../utils/responses')

async function createSliderMovil (req, res) {
  try {
    await sSliderMovil.createSliderMovil(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I04401')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllSliderMovil (req, res) {
  try {
    const sliderMovil = await sSliderMovil.getAllSliderMovil()
    responses.makeResponseOk(res, { sliderMovil }, 'sliderMovil/getAllSliderMovil')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getSliderMovil (req, res) {
  try {
    const id = req.params.id
    const sliderMovil = await sSliderMovil.getSliderMovil(id)
    responses.makeResponseOk(res, { sliderMovil }, 'sliderMovil/getSliderMovil')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateSliderMovil (req, res) {
  try {
    const sliderMovil = await sSliderMovil.updateSliderMovil(req.params.id, req.fields)
    responses.makeResponseOk(res, { sliderMovil }, 'sliderMovil/getSliderMovil')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteSliderMovil (req, res) {
  try {
    await sSliderMovil.deleteSliderMovil(req.params.id)
    responses.makeResponseOkMessage(res, 'I04403')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = { createSliderMovil,
  getAllSliderMovil,
  getSliderMovil,
  updateSliderMovil,
  deleteSliderMovil }

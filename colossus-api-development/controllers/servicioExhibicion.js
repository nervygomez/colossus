const sServicioExhibicion = require('../services/servicioExhibicion')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function getAllServicioExhibicion (req, res) {
  try {
    const servicioExhibicion = await sServicioExhibicion.getAllServicioExhibicion()
    responses.makeResponseOk(res, { servicioExhibicion }, 'servicioExhibicion/getAllServicioExhibicion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateServicioExhibicion (req, res) {
  try {
    const img = isEmpty(req.fields) ? null : req.fields.imagen
    await sServicioExhibicion.updateServicioExhibicion(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I-servicioExhibicion-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  getAllServicioExhibicion,
  updateServicioExhibicion
}

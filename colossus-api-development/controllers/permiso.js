
const sPermiso = require('../services/permiso')
const responses = require('../utils/responses')

async function getAllPermiso (req, res) {
  try {
    const permiso = await sPermiso.getAllPermiso()
    responses.makeResponseOk(res, { permiso }, 'permiso/getAllPermiso')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
/*
async function getPermiso (req, res) {
  try {
    const id = req.params.id
    const permiso = await sPermiso.getPermiso(id)
    responses.makeResponseOk(res, { permiso }, 'permiso/getPermiso')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
*/

module.exports = {
  // getPermiso,
  getAllPermiso
}


const sTipoEquipo = require('../services/tipoEquipo')
const responses = require('../utils/responses')
const isEmpty = require('../utils/helpers').isEmpty

async function createTipoEquipo (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sTipoEquipo.addTipoEquipo(req.fields, img)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I00301')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllTipoEquipo (req, res) {
  try {
    const tipoEquipo = await sTipoEquipo.getAllTipoEquipo()
    responses.makeResponseOk(res, { tipoEquipo }, 'tipoEquipo/getAllTipoEquipo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getTipoEquipo (req, res) {
  try {
    const id = req.params.id
    const tipoEquipo = await sTipoEquipo.getTipoEquipo(id)
    responses.makeResponseOk(res, { tipoEquipo }, 'tipoEquipo/getTipoEquipo')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoEquipo (req, res) {
  try {
    const img = isEmpty(req.files) ? null : req.files.imagen
    await sTipoEquipo.updateTipoEquipo(req.params.id, req.fields, img)
    responses.makeResponseOkMessage(res, 'I00302')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoEquipo (req, res) {
  try {
    await sTipoEquipo.deleteTipoEquipo(req.params.id)
    responses.makeResponseOkMessage(res, 'I00303')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

async function getMarcasTipoEquipo (req, res) {
  try {
    const id = req.params.id
    const marca = await sTipoEquipo.getMarcasTipoEquipo(id)
    responses.makeResponseOk(res, { marca }, 'marca/getAllMarca')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getCatalogosTipoEquipo (req, res) {
  try {
    const id = req.params.id
    const catalogoServicio = await sTipoEquipo.getCatalogosTipoEquipo(id)
    responses.makeResponseOk(res, { catalogoServicio }, 'catalogoServicio/getAllCatalogoServicio')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

module.exports = {
  createTipoEquipo,
  getTipoEquipo,
  getAllTipoEquipo,
  updateTipoEquipo,
  deleteTipoEquipo,
  getMarcasTipoEquipo,
  getCatalogosTipoEquipo
}

const sTecnicoExhibicion = require('../services/tecnicoExhibicion')
const responses = require('../utils/responses')

async function getAllTecnicoExhibicion (req, res) {
  try {
    const tecnicoExhibicion = await sTecnicoExhibicion.getAllTecnicoExhibicion()
    responses.makeResponseOk(res, { tecnicoExhibicion }, 'tecnicoExhibicion/getAllTecnicoExhibicion')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTecnicoExhibicion (req, res) {
  try {
    await sTecnicoExhibicion.updateTecnicoExhibicion(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I-tecnicoExhibicion-01')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
module.exports = {
  getAllTecnicoExhibicion,
  updateTecnicoExhibicion
}


const sTipoCaracteristicaCliente = require('../services/tipoCaracteristicaCliente')
const responses = require('../utils/responses')

async function createTipoCaracteristicaCliente (req, res) {
  try {
    await sTipoCaracteristicaCliente.addTipoCaracteristicaCliente(req.fields)
    console.log(req.fields)
    responses.makeResponseOkMessage(res, 'I02401')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function getAllTipoCaracteristicaCliente (req, res) {
  try {
    const tipoCaracteristicaCliente = await sTipoCaracteristicaCliente.getAllTipoCaracteristicaCliente()
    responses.makeResponseOk(res, { tipoCaracteristicaCliente }, 'tipoCaracteristicaCliente/getAllTipoCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}
async function getTipoCaracteristicaCliente (req, res) {
  try {
    const id = req.params.id
    const tipoCaracteristicaCliente = await sTipoCaracteristicaCliente.getTipoCaracteristicaCliente(id)
    responses.makeResponseOk(res, { tipoCaracteristicaCliente }, 'tipoCaracteristicaCliente/getTipoCaracteristicaCliente')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function updateTipoCaracteristicaCliente (req, res) {
  try {
    await sTipoCaracteristicaCliente.updateTipoCaracteristicaCliente(req.params.id, req.fields)
    responses.makeResponseOkMessage(res, 'I02402')
  } catch (e) {
    responses.makeResponseException(res, e)
  }
}

async function deleteTipoCaracteristicaCliente (req, res) {
  try {
    await sTipoCaracteristicaCliente.deleteTipoCaracteristicaCliente(req.params.id)
    responses.makeResponseOkMessage(res, 'I02403')
  } catch (e) {
    responses.makeResponseException(res, e)
    console.error(e)
  }
}

module.exports = {
  createTipoCaracteristicaCliente,
  getTipoCaracteristicaCliente,
  getAllTipoCaracteristicaCliente,
  updateTipoCaracteristicaCliente,
  deleteTipoCaracteristicaCliente
}

'use strict'
const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  const Comentario = sequelize.define('Comentario', {
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede enviar un comentario vacio'
        }
      }
    },
    respuesta: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    fechaRespuesta: {
      type: DataTypes.DATE,
      defaultValue: null,
      get: function () {
        if (this.getDataValue('fechaRespuesta') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRespuesta')).format('DD-MM-YYYY hh:mm')
      }
    },
    correo: {
      type: DataTypes.STRING,
      defaultValue: null
    }
  },
  {
    tableName: 'comentario',
    name: {
      singular: 'comentario',
      plural: 'comentarios'
    },
    getterMethods: {
      createdAt () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY hh:mm')
      },
      dayCreated () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY')
      }
    }
  })
  Comentario.associate = (models) => {
    Comentario.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
    Comentario.belongsTo(models.TipoComentario, { foreignKey: 'idTipoComentario' })
  }
  return Comentario
}

'use strict'
module.exports = (sequelize, DataTypes) => {
  const PreguntaCalificacion = sequelize.define('PreguntaCalificacion', {
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripcion vacia.'
        }
      }
    }
  },
  {
    tableName: 'preguntaCalificacion',
    name: {
      singular: 'preguntaCalificacion',
      plural: 'preguntasCalificacion'
    }
  })
  PreguntaCalificacion.associate = (models) => {
    PreguntaCalificacion.hasMany(models.CalificacionServicio, { foreignKey: 'idPreguntaCalificacion' })
  }
  return PreguntaCalificacion
}

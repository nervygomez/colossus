'use strict'
const moment = require('moment')
module.exports = (sequelize, DataTypes) => {
  const Garantia = sequelize.define('Garantia', {
    fechaExpiracion: {
      type: DataTypes.DATE,
      get: function () {
        if (this.getDataValue('fechaExpiracion') === null) {
          return null
        }
        return moment(this.getDataValue('fechaExpiracion')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaExpiracion', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      }
    }
  },
  {
    tableName: 'garantia',
    name: {
      singular: 'garantia',
      plural: 'garantias'
    },
    getterMethods: {
      createdAt () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY')
      },
      vigente () {
        const fechaE = moment(this.getDataValue('fechaExpiracion'))
        if (fechaE.isSameOrAfter(moment().endOf('day'))) {
          return 'Activa'
        } else {
          return 'Vencida'
        }
      }
    }
  })
  Garantia.associate = (models) => {
    Garantia.belongsToMany(models.Condicion, { through: 'condicionGarantia' })
    Garantia.belongsTo(models.OrdenServicio, { foreignKey: 'idOrdenServicio' })
    Garantia.belongsTo(models.DuracionGarantia, { foreignKey: 'idDuracionGarantia' })
  }
  return Garantia
}

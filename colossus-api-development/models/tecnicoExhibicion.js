'use stric'
module.exports = (sequelize, DataTypes) => {
  const TecnicoExhibicion = sequelize.define('TecnicoExhibicion', {
    descripcion: DataTypes.STRING
  },
  {
    tableName: 'tecnicoExhibicion',
    name: {
      singular: 'tecnicoExhibicion',
      plural: 'tecnicosExhibicion'
    }
  })
  TecnicoExhibicion.associate = (models) => {
    TecnicoExhibicion.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
  }
  return TecnicoExhibicion
}

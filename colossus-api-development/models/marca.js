'use strict'

module.exports = (sequelize, DataTypes) => {
  const Marca = sequelize.define('Marca', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  // options
  {
    tableName: 'marca',
    name: {
      singular: 'marca',
      plural: 'marcas'
    }
  })
  Marca.associate = function (models) {
    Marca.hasMany(models.ModeloEquipo, { foreignKey: 'idMarca' })
  }
  return Marca
}

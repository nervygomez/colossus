'use strict'

const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const SolicitudServicio = sequelize.define('SolicitudServicio', {
    fechaRespuesta: {
      type: DataTypes.DATE,
      defaultValue: null,
      get: function () {
        if (this.getDataValue('fechaRespuesta') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRespuesta')).format('DD-MM-YYYY hh:mm:ss')
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'Debe tener una descripcion del motivo de la solicitud.'
        }
      }
    },
    fechaRevisado: {
      type: DataTypes.DATE,
      defaultValue: null,
      get: function () {
        if (this.getDataValue('fechaRevisado') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRevisado')).format('DD-MM-YYYY hh:mm:ss')
      }
    },
    fechaGeneracionPresupuesto: {
      type: DataTypes.DATE,
      defaultValue: null,
      get: function () {
        if (this.getDataValue('fechaGeneracionPresupuesto') === null) {
          return null
        }
        return moment(this.getDataValue('fechaGeneracionPresupuesto')).format('DD-MM-YYYY hh:mm:ss')
      }
    },
    presupuesto: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null

    },
    estatusPresupuesto: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    estatus: {
      type: DataTypes.STRING,
      defaultValue: 'E'
    }
  },
  {
    tableName: 'solicitudServicio',
    name: {
      singular: 'solicitudServicio',
      plural: 'solicitudesServicio'
    },
    getterMethods: {
      fechaCreacion () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY')
      },
      fechaRevisado2 () {
        if (this.getDataValue('fechaRevisado') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRevisado')).format('DD-MM-YYYY')
      }
    }
  })
  SolicitudServicio.associate = (models) => {
    SolicitudServicio.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
    SolicitudServicio.belongsTo(models.ModeloEquipo, { foreignKey: 'idModeloEquipo' })
    SolicitudServicio.belongsTo(models.CatalogoServicio, { foreignKey: 'idCatalogoServicio' })
    SolicitudServicio.belongsTo(models.Promocion, { foreignKey: 'idPromocion' })
    SolicitudServicio.hasMany(models.Bitacora, { foreignKey: 'idSolicitudServicio' })
    SolicitudServicio.hasMany(models.Notificacion, { foreignKey: 'idSolicitudServicio' })
    SolicitudServicio.hasOne(models.OrdenServicio, { foreignKey: 'idSolicitudServicio' })
    SolicitudServicio.hasOne(models.Revision, { foreignKey: 'idSolicitudServicio' })
    SolicitudServicio.belongsTo(models.MotivoRechazo, { foreignKey: 'idMotivoRechazo' })
  }
  return SolicitudServicio
}

'use strict'

module.exports = (sequelize, DataTypes) => {
  const ModeloEquipo = sequelize.define('ModeloEquipo', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'El campo nombre no puede estar vacio'
        }
      }
    }
  },
  {
    tableName: 'modeloequipo',
    name: {
      singular: 'modeloEquipo',
      plural: 'modelosEquipo'
    }
  })
  ModeloEquipo.associate = function (models) {
    ModeloEquipo.belongsTo(models.TipoEquipo, { foreignKey: 'idTipoEquipo' })
    ModeloEquipo.belongsTo(models.Marca, { foreignKey: 'idMarca' })
    ModeloEquipo.hasMany(models.SolicitudServicio, { foreignKey: 'idModeloEquipo' })
  }
  return ModeloEquipo
}

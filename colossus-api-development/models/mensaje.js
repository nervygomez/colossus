'use strict'

module.exports = (sequelize, DataTypes) => {
  const Mensaje = sequelize.define('Mensaje', {
    descripcion: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener una descripcion vacía.'
        }
      }
    },
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener un nombre vacío.'
        }
      }
    }
  },
  {
    tableName: 'mensaje',
    name: {
      plural: 'mensajes',
      singular: 'mensaje'
    }
  })
  Mensaje.associate = function (models) {
  }
  return Mensaje
}

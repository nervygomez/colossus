'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoCaracteristicaCliente = sequelize.define('TipoCaracteristicaCliente', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    }
  },
  {
    tableName: 'tipoCaracteristicaCliente',
    name: {
      singular: 'tipoCaracteristicaCliente',
      plural: 'tiposCaracteristicaCliente'
    }

  })
  TipoCaracteristicaCliente.associate = (models) => {
    TipoCaracteristicaCliente.hasMany(models.CaracteristicaCliente, { foreignKey: 'idTipoCaracteristicaCliente' })
  }

  return TipoCaracteristicaCliente
}

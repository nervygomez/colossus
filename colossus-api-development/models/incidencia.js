'use strict'
const moment = require('moment')
module.exports = (sequelize, Datatypes) => {
  const Incidencia = sequelize.define('Incidencia', {
    descripcion: {
      type: Datatypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripcion vacia.'
        }
      }
    }
  },
  {
    tableName: 'incidencia',
    name: {
      singular: 'incidencia',
      plural: 'incidencias'
    },
    getterMethods: {
      fechaCreacion () {
        return moment(this.getDataValue('createdAt')).format('DD-MM-YYYY')
      }
    }
  })

  Incidencia.associate = (models) => {
    Incidencia.belongsTo(models.TipoIncidencia, { foreignKey: 'idTipoIncidencia' })
    Incidencia.belongsTo(models.Agenda, { foreignKey: 'idAgenda' })
  }

  return Incidencia
}

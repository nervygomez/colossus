'use strict'
module.exports = (sequelize, DataTypes) => {
  const DuracionGarantia = sequelize.define('DuracionGarantia', {
    dia: DataTypes.INTEGER
  },
  {
    tableName: 'duracionGarantia',
    name: {
      singular: 'duracionGarantia',
      plural: 'duracionesGarantia'
    }

  })
  DuracionGarantia.associate = (models) => {
    DuracionGarantia.hasMany(models.Garantia, { foreignKey: 'idDuracionGarantia' })
  }

  return DuracionGarantia
}

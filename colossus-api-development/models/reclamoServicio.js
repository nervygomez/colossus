'use strict'
const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
  const ReclamoServicio = sequelize.define('ReclamoServicio', {
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    fechaRespuesta: {
      type: DataTypes.DATEONLY,
      get: function () {
        if (this.getDataValue('fechaRespuesta') === null) {
          return null
        }
        return moment(this.getDataValue('fechaRespuesta')).format('DD-MM-YYYY')
      },
      set: function (val) {
        this.setDataValue('fechaRespuesta', moment(val, 'DD-MM-YYYY').format('YYYY-MM-DD'))
      }
    },
    estatus: {
      type: DataTypes.STRING,
      defaultValue: 'En espera'
    },
    estatusRevision: {
      type: DataTypes.STRING,
      defaultValue: null
    },
    diagnostico: {
      type: DataTypes.STRING(500),
      defaultValue: null
    }
  }, {
    tableName: 'reclamoServicio',
    name: {
      plural: 'reclamosServicio',
      singular: 'reclamoServicio'
    }
  })
  ReclamoServicio.associate = function (models) {
    ReclamoServicio.belongsTo(models.OrdenServicio, { foreignKey: 'idOrdenServicio' })
    ReclamoServicio.belongsTo(models.TipoReclamo, { foreignKey: 'idTipoReclamo' })
    ReclamoServicio.belongsTo(models.MotivoRechazo, { foreignKey: 'idMotivoRechazo' })
    ReclamoServicio.belongsToMany(models.Actividad, { through: 'tareaReclamo' })
    ReclamoServicio.belongsTo(models.Usuario, { foreignKey: 'idUsuario' })
  }
  return ReclamoServicio
}

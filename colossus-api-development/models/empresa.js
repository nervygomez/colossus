'use strict'

module.exports = (sequelize, DataTypes) => {
  const Empresa = sequelize.define('Empresa', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacio.'
        }
      }
    },
    rif: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener rif vacio.'
        }
      }
    },
    telefono: DataTypes.STRING,
    facebook: DataTypes.STRING,
    twitter: DataTypes.STRING,
    instagram: DataTypes.STRING,
    direccion: DataTypes.STRING,
    correo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener correo vacio.'
        }
      }
    },
    mision: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener misión vacía'
        }
      }
    },
    vision: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener misión vacía'
        }
      }
    },
    objetivoGeneral: {
      type: DataTypes.STRING(2000),
      validate: {
        notEmpty: {
          msg: 'No puede tener misión vacía'
        }
      }
    },
    urlLogo: DataTypes.STRING(2000),
    urlImagenMision: DataTypes.STRING(2000),
    urlImagenVision: DataTypes.STRING(2000),
    urlImagenObjetivoGeneral: DataTypes.STRING(2000),
    urlFavIco: DataTypes.STRING(2000),

    cargada: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }

  },
  {
    tableName: 'empresa',
    name: {
      singular: 'Empresa',
      plural: 'Empresas'
    }
  })
  Empresa.associate = (models) => {
  }
  return Empresa
}

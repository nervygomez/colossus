'use strict'
module.exports = (sequelize, DataTypes) => {
  const TipoEquipo = sequelize.define('TipoEquipo', {
    nombre: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener nombre vacío.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  // opciones
  {
    tableName: 'tipoEquipo',
    name: {
      singular: 'tipoEquipo',
      plural: 'tiposEquipo'
    }
  })
  TipoEquipo.associate = function (models) {
    TipoEquipo.belongsTo(models.CategoriaTipoEquipo, { foreignKey: 'idCategoriaTipoEquipo' })
    TipoEquipo.hasMany(models.ModeloEquipo, { foreignKey: 'idTipoEquipo' })
    TipoEquipo.hasMany(models.CatalogoServicio, { foreignKey: 'idTipoEquipo' })
  }
  return TipoEquipo
}

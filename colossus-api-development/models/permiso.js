'use strict'

module.exports = (sequelize, DataTypes) => {
  const Permiso = sequelize.define('Permiso', {
    nombre: DataTypes.STRING
  }, {
    tableName: 'permiso',
    name: {
      singular: 'permiso',
      plural: 'permisos'
    }
  })
  Permiso.associate = function (models) {
    Permiso.belongsToMany(models.Rol, { through: 'permisoRol' })
  }
  return Permiso
}

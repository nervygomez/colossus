'use strict'
module.exports = (sequelize, DataTypes) => {
  const HorarioTrabajo = sequelize.define('HorarioTrabajo', {
    horaInicio: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Debe tener hora inicio.'
        }
      }
    },
    horaFinal: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Debe tener hora final.'
        }
      }
    },
    horaInicioFeriado: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Debe tener hora inicio feriado.'
        }
      }
    },
    horaFinalFeriado: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Debe tener hora final feriado.'
        }
      }
    }
  },
  {
    tableName: 'horarioTrabajo',
    name: {
      singular: 'horarioTrabajo',
      plural: 'horarioTrabajo'
    }

  })
  HorarioTrabajo.associate = (models) => {
  }

  return HorarioTrabajo
}

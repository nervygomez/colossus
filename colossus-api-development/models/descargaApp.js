'use strict'

module.exports = (sequelize, DataTypes) => {
  const DescargaApp = sequelize.define('DescargaApp', {
    titulo: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener título vacío.'
        }
      }
    },
    descripcion: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          msg: 'No puede tener la descripción vacía.'
        }
      }
    },
    urlImagen: DataTypes.STRING(2000)
  },
  {
    tableName: 'descargaApp',
    name: {
      singular: 'descargaApp',
      plural: 'descargaApps'
    }
  })
  DescargaApp.associate = (models) => {

  }
  return DescargaApp
}

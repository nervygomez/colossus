import { Component, OnInit } from '@angular/core';
import {FormControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ServicesService} from '../services/services.service';
import * as moment from 'moment';

@Component({
  selector: 'app-suscribirse',
  templateUrl: './suscribirse.component.html',
  styleUrls: ['./suscribirse.component.css'],
  providers: [ServicesService]
})
export class SuscribirseComponent implements OnInit {
  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  existe: boolean;
  mensaje: string;

  // tslint:disable-next-line:variable-name
  constructor(private _formBuilder: FormBuilder, private serv: ServicesService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      nombrectrl : ['',
      Validators.required
    ],
    apellidoctrl : ['',
      Validators.required
    ],
    fechactrl : ['',
      Validators.required
    ],
    sexoctrl : ['',
      Validators.required
    ]

    });
    this.secondFormGroup = this._formBuilder.group({
      correoctrl : ['',
      Validators.required]
    });
  }
  // tslint:disable-next-line:member-ordering
  nombrectrl = new FormControl('',
  [
    Validators.required
  ]
  );
  // tslint:disable-next-line:member-ordering
  apellidoctrl = new FormControl('',
  [
    Validators.required
  ]
  );
  // tslint:disable-next-line:member-ordering
  correoctrl = new FormControl('',
  [
    Validators.required
  ]
  );
  // tslint:disable-next-line:member-ordering
  fechactrl = new FormControl('',
  [
    Validators.required
  ]
  );
  // tslint:disable-next-line:member-ordering
  sexoctrl = new FormControl('',
  [
    Validators.required
  ]
  );

  onSubscribe() {
    const datos = new FormData();
    const email = new FormData();
    if (this.secondFormGroup.get('correoctrl').valid) {
      email.append('correo', this.secondFormGroup.get('correoctrl').value);
    }
    if ( this.firstFormGroup.get('nombrectrl').valid && this.firstFormGroup.get('apellidoctrl').valid &&
        this.firstFormGroup.get('sexoctrl').valid && this.firstFormGroup.get('fechactrl').valid &&
        this.secondFormGroup.get('correoctrl').valid) {
        datos.append('nombre', this.firstFormGroup.get('nombrectrl').value);
        datos.append('apellido', this.firstFormGroup.get('apellidoctrl').value);
        datos.append('fechaNacimiento', moment(this.firstFormGroup.get('fechactrl').value).format('DD/MM/YYYY'));
        datos.append('correo', this.secondFormGroup.get('correoctrl').value);
        datos.append('sexo', this.firstFormGroup.get('sexoctrl').value);
        console.log(this.firstFormGroup.get('sexoctrl').value);
      }

    this.serv.guardarSubscripcion(datos).subscribe( resp => {
        console.log(resp);
        this.existe = true;
        this.mensaje = 'Ahora estas suscrito a nuestro sistema. Verifica tu correo y sigue las instucciones.';
      }, (err) => {
        if (err.error.data.message === 'Ya existe un usuario con ese correo.') {
        this.existe = false;
        this.mensaje = 'Este usuario ya existe, por favor introduzca otro correo.';
        }
      });
  }


}

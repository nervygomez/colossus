import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';

interface Catalogo {
  categoria: string;
  imagenCategoria: any;
  tipoEquipo: string;
  tipoServicio: any;
  actividades ?: any;
}

@Component({
  selector: 'app-tiposervicios',
  templateUrl: './tiposervicios.component.html',
  styleUrls: ['./tiposervicios.component.css'],
  providers: [ServicesService]
})
export class TiposerviciosComponent implements OnInit {
  catalogosExhi2:[];
  catalogosExhi: Catalogo[] = [
  ];
  catalogos: Catalogo[] = [
  ];
  categoria: any;
  servicios: any;
  catal: any;
  textFilter: any;
  filterCatalogo: any[] = [];
  search = false;

  constructor(private serv: ServicesService) { }

  ngOnInit() {

    this.serv.obtenerCatalagoEnExhibicion().subscribe( (res: any) => {
      this.catal = res.data;
      if (this.catal.length > 0) {
        this.catal.forEach((element: any) => {
          this.catalogosExhi.push({
            categoria: element.catalogoServicio.tipoEquipo.categoriaTipoEquipo.nombre,
            imagenCategoria: element.catalogoServicio.urlImagen,
            tipoEquipo: element.catalogoServicio.tipoEquipo.nombre,
            tipoServicio: element.catalogoServicio.tipoServicio.nombre,
            actividades: element.catalogoServicio.actividades,
          });
        });
      }
    });
}

}

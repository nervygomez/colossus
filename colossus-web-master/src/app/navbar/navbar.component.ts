import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {ServicesService} from '../services/services.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [ServicesService]
})
export class NavbarComponent implements OnInit {
  empresa = {};
  url: any;
  @Output() public sidenavToggle = new EventEmitter();

  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerEmpresa().subscribe( resp => {
      console.log(resp);
      this.empresa = resp.data;
    });
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  abrirlogin(url) {
    this.url = 'http://localhost:4201/login';
    console.log(this.url);
    window.open(this.url, '_blank');
  }


}

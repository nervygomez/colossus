import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { SuscribirseComponent } from '../app/suscribirse/suscribirse.component';
import { InicioComponent } from '../app/inicio/inicio.component';
import { NosotrosComponent } from '../app/nosotros/nosotros.component';
import { ConocenosComponent } from '../app/conocenos/conocenos.component';
import { PromocionesComponent } from '../app/promociones/promociones.component';
import { CatalogoComponent } from '../app/catalogo/catalogo.component';

const routes: Routes = [
    { path: '', component: InicioComponent },
    { path: 'home', component: InicioComponent },
    { path: 'suscribirse', component: SuscribirseComponent },
    { path: 'nosotros', component: NosotrosComponent },
    { path: 'conocenos', component: ConocenosComponent },
    { path: 'promociones', component: PromocionesComponent },
    { path: 'catalogo', component: CatalogoComponent },
    { path: '**', component: InicioComponent },
 ];

@NgModule({
     imports: [RouterModule.forRoot(routes)],
     exports: [RouterModule]
 })

 export class AppRoutingModule { }

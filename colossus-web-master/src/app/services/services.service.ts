import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaderResponse} from '@angular/common/http';

const url = 'http://68.183.117.24:8900/';

@Injectable()
export class ServicesService {
  constructor(public http: HttpClient) {

  }

  guardarSubscripcion( data: any) {
   return  this.http.post<any>(url + 'cliente', data);
  }

  obtenerMarca() {
    return this.http.get<any>(url + 'marca');
  }
  obtenerEmpleados() {
    return this.http.get<any>(url + 'tecnicoExhibicion');
  }
  obtenerCatalogo() {
    return this.http.get<any>(url + 'catalogoServicio');
  }
  obtenerCatalagoEnExhibicion() {
    return this.http.get<any>(url + 'servicioExhibicion');
  }
  obtenerCategorias() {
    return this.http.get<any>(url + 'tipoEquipo');
  }
  obtenerActividades(data: any) {
    return this.http.get<any>(url + 'catalogoServicio/' + data );
  }
 obtenerEmpresa() {
    return this.http.get<any>(url + 'empresa');
 }
 obtenerHorarioTrabajo() {
  return this.http.get<any>(url + 'horarioTrabajo');
}
 obtenerTipoComentario() {
  return this.http.get<any>(url + 'tipocomentario');
}
obtenerPromociones() {
  return this.http.get<any>(url + 'promocion');
}

obtenerPreguntas() {
  return this.http.get<any>(url + 'preguntaFrecuente');
}

obteneCaracteristicasEmpresa() {
  return this.http.get<any>(url + 'caracteristicaEmpresa');
}

obtenerDescargaApp() {
  return this.http.get<any>(url + 'descargaApp');
}

obtenerCarrusel() {
  return this.http.get<any>(url + 'carrusel');
}

guardarSugerencia( data: any) {
  return  this.http.post<any>(url + 'comentario/usuarioNoRegistrado', data);
 }
}



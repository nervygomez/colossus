import { Component, OnInit } from '@angular/core';
import {ServicesService} from '../services/services.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-conocenos',
  templateUrl: './conocenos.component.html',
  styleUrls: ['./conocenos.component.css'],
  providers: [ServicesService]
})
export class ConocenosComponent implements OnInit {
  empleados: any;

  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerEmpleados().subscribe( resp => {
      console.log(resp);
      this.empleados = resp.data;
      console.log(this.empleados[0].nombre);
      $(function() {
        $('.material-card > .mc-btn-action').click(function () {
            var card = $(this).parent('.material-card');
            var icon = $(this).children('i');
            icon.addClass('fa-spin-fast');

            if (card.hasClass('mc-active')) {
                card.removeClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-arrow-left')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-bars');

                }, 800);
            } else {
                card.addClass('mc-active');

                window.setTimeout(function() {
                    icon
                        .removeClass('fa-bars')
                        .removeClass('fa-spin-fast')
                        .addClass('fa-arrow-left');

                }, 800);
            }
        });
    });
    });
  }

}

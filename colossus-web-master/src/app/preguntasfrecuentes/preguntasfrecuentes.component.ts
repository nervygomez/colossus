import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services/services.service';

@Component({
  selector: 'app-preguntasfrecuentes',
  templateUrl: './preguntasfrecuentes.component.html',
  styleUrls: ['./preguntasfrecuentes.component.css'],
  providers: [ServicesService]
})
export class PreguntasfrecuentesComponent implements OnInit {
  preguntas: any[] = [];

  constructor(private serv: ServicesService) { }

  ngOnInit() {
    this.serv.obtenerPreguntas().subscribe( resp => {
      console.log(resp);
      this.preguntas = resp.data;
    })
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { ServicesService } from '../services/services.service';

import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

import * as M from '../../assets/js/materialize.min.js';
import * as $ from 'jquery';

interface Catalogo {
  categoria: string;
  imagenCategoria: any;
  tipoEquipo: string;
  tipoServicio: any;
  actividades ?: any;
}


@Component({
  selector: 'app-botton-sheet-actividades',
  templateUrl: 'bottonSheetActividades.html',
  providers: [MatBottomSheet],
})
export class BottomSheetOverviewExampleSheetComponent implements OnInit {
  ct: any;
  dataSheet: any;
  instances: any;
  constructor(public bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewExampleSheetComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    this.dataSheet = data;
    console.log(this.dataSheet);
  }
  ngOnInit(): void {

  }

  openLink() {
    this.bottomSheetRef.afterDismissed().subscribe(() => {
      console.log('Bottom sheet has been dismissed.');
    });
    this.bottomSheetRef.dismiss();
  }
}

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css'],
  providers: [ServicesService, MatBottomSheet]
})

export class CatalogoComponent implements OnInit {

  catalogosExhi: Catalogo[] = [
  ];
  catalogos: Catalogo[] = [
  ];
  categoria: any;
  servicios: any;
  catal: any;
  textFilter: any;
  filterCatalogo: any[] = [];
  search = false;

  constructor(private serv: ServicesService, private bottomSheet: MatBottomSheet) {
  }

  ngOnInit() {
    this.obtenerCatalagosEnExhibicion();
    this.obtenerCatalagos();
  }

  openBottomSheet(item: any): void {
    this.bottomSheet.open(BottomSheetOverviewExampleSheetComponent, {
      data: item,
      closeOnNavigation: true,
      disableClose: false,
      hasBackdrop: true,
    });
  }

  private obtenerCatalagosEnExhibicion() {
        this.serv.obtenerCatalagoEnExhibicion().subscribe( (res: any) => {
          this.catal = res.data;
          if (this.catal.length > 0) {
            this.catal.forEach((element: any) => {
              this.catalogosExhi.push({
                categoria: element.catalogoServicio.tipoEquipo.categoriaTipoEquipo.nombre,
                imagenCategoria: element.catalogoServicio.urlImagen,
                tipoEquipo: element.catalogoServicio.tipoEquipo.nombre,
                tipoServicio: element.catalogoServicio.tipoServicio.nombre,
                actividades: element.catalogoServicio.actividades,
              });
            });
            console.log('ex',this.catalogosExhi);
          }
        });
  }
  private obtenerCatalagos() {
    this.serv.obtenerCategorias().subscribe(resp => {
      this.categoria = resp.data;
      if (this.categoria.length !== 0) {
        this.serv.obtenerCatalogo().subscribe(res => {
          this.catal = res.data;
          if (this.catal.length !== 0) {
            this.categoria.forEach(element => {

              this.catalogos.push({
                categoria: element.categoriaTipoEquipo.nombre,
                imagenCategoria: element.urlImagen,
                tipoEquipo: element.nombre,
                tipoServicio: this.obtenerServicio(element.id, this.catal),
              });

            });
            console.log('CATALOGOS', this.catalogos);
            this.filterCatalogo = this.catalogos;
          }
        });
      }
    });
  }

  obtenerServicio(id: any, arr: any) {

    return arr.find((element, index, array) => {
      if (element.tipoEquipo) {
        console.log('element', element.tipoEquipo);
        return element.tipoEquipo.id === id;
      }
      console.log('element', false);
      return false;
    });
  }

  OnClick(equipo: any) {
    this.categoria = [];

    console.log(equipo.categoriaTipoEquipo.id);

    this.serv.obtenerActividades(equipo.categoriaTipoEquipo.id).subscribe(resp => {
      const info  = resp;
      console.log(info);
    });
  }

  filtrarCatalogo() {
    this.search = true;
    // texto buscado en minusculas
    const tx = this.textFilter.toLowerCase();
    // expresion regular para encontrar concidencias en el array
    const regex = new RegExp('\(\^' + tx + '\)+', 'm');
    if (tx === '') {
      this.search = false;
      this.filterCatalogo = this.catalogos;

    } else {
      // Filtra el array de acuerdo al texto buscado
      this.filterCatalogo = this.catalogos.filter((item) => {
        if (
          regex.exec(item.categoria.toLowerCase()) ||
          regex.exec(item.tipoEquipo.toLowerCase()) ||
          regex.exec(item.tipoServicio ? item.tipoServicio.tipoServicio.nombre.toLowerCase() : '')
        ) {
          return true;
        }
      });
    }
  }
openPromocionModal(id) {
  const elems = document.querySelectorAll('#' + id);
  const instances = M.Modal.init(elems);
  instances.open();
}

}

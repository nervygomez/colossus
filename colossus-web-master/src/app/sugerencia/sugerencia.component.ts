import { Component, OnInit } from '@angular/core';
import * as M from '../../assets/js/materialize.min.js';
import { ServicesService } from '../services/services.service';
import { MatRadioModule } from '@angular/material/radio';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-sugerencia',
  templateUrl: './sugerencia.component.html',
  styleUrls: ['./sugerencia.component.css'],
  providers: [ServicesService]
})
export class SugerenciaComponent implements OnInit {

  tipoComentario: any = [];

  constructor(private serv: ServicesService,
              private _matradiomodule: MatRadioModule) {
              }

formSugerencias: FormGroup;
instances: any;
  ngOnInit() {

    this.formSugerencias = new FormGroup({
      descripcion: new FormControl('', [
        Validators.required
      ]),
      idTipoComentario: new FormControl('', [
        Validators.required
      ]),
      correo: new  FormControl('', [
        Validators.required
      ])
    });

    this.obtenerTipoComentario();
    console.log(this.tipoComentario);
    const elems = document.querySelectorAll('.modal');
    this.instances = M.Modal.init(elems);

  }

  public guardarSugerencia() {
    if (this.formSugerencias.valid) {
      this.serv.guardarSugerencia(this.formSugerencias.value)
      .subscribe((resp) => {
        console.log(resp);
        M.toast({html: 'Comentario Enviado, Gracias.', classes: 'rounded green'});
        setTimeout(() => {
          this.instances[0].close();

        }, 2000);
      });
    } else {
      M.toast({html: 'Complete todos los Campos.', classes: 'rounded red'});

    }
  }


  private obtenerTipoComentario() {
      this.serv.obtenerTipoComentario().subscribe(resp => {
        this.tipoComentario = resp.data;
        console.log(this.tipoComentario);
      });
  }
}
